Next-Generation Online Bazaar is my last creation, developed in 2021.

In a nutshell, it's basically an eCommerce, micro-services based web application, built with a wide stack of technologies and available for deployment using minikube.

Available in english, german and romanian languages, it supports the main features one expect from such a platform, including:

• registration and login -> social login with Facebook/Google also included;

• products of all sorts -> viewing, searching, sorting, buying and reviewing;

• personal account settings, order list and shopping cart;

• admin dashboard for platform management;

• timed promotions;

Developed with the following modern, enterprise full-stack, hence the "next-generation" in the project name: 

• Angular (11)

--> Bootstrap, Flexbox, Material, RxJS, CLI

• Java (11.0.8)

--> Spring Boot (2.3.5)

• PostgreSQL (11.7)

--> Hibernate (5.4)

--> ElasticSearch (7.9)

• Camel (3.2)

--> RabbitMQ (3.5)

• Redis (6.2)

• Docker (19.03)

--> Testcontainers (1.14.3)

• Kubernetes (1.20)

--> Helm (3)

For step by step installation, please check the "k8s/Setup.txt" file.