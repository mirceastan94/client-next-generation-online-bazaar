export const environment = {
  production: false,
  productsApiUrl: 'http://localhost:8080/product',
  ordersApiUrl: 'http://localhost:8081/order',
  stripe: 'pk_test_51JbOsXFo8MAeFh5iW9LKryur355pG6Kay9ya7crAoTK8GqlskJgVL7rVVBWw7vFsW8ri0693REiXSqTRkjA1djSU00EHuNAdAn',
  serverUrl: '/api'
};
