export const environment = {
  production: true,
  productsApiUrl: 'https://ngob.info/product',
  ordersApiUrl: 'https://ngob.info/order',
  stripe: 'pk_test_51JbOsXFo8MAeFh5iW9LKryur355pG6Kay9ya7crAoTK8GqlskJgVL7rVVBWw7vFsW8ri0693REiXSqTRkjA1djSU00EHuNAdAn',
  serverUrl: '/api'
};
