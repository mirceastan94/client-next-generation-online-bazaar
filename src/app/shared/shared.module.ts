import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogService } from './logging/log.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faFacebook, faGoogle, faProductHunt, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { MarkAsteriskDirective } from './directives/mark-asterisk.directive';
import { RoundPipe } from './pipes/round.pipe';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MaterialModule } from './material/material.module';
import { NgxTransService } from './translate/translate.service';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import { faAd, faAddressBook, faAngleDown, faAngleRight, faArrowCircleDown, faArrowCircleUp, faBan, faBars, faBox, faChartBar, faChartPie, faCheckSquare, faEdit, faEnvelope, faExclamationTriangle, faFilePdf, faFilter, faLocationArrow, faLock, faMinusSquare, faPhone, faPowerOff, faQuestion, faRocket, faSearch, faShoppingBag, faSpinner, faStar, faSync, faTachometerAlt, faTag, faTags, faTimes, faTrash, faTruck, faUser, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { TranslateComponent } from './translate/translate.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { BarRatingModule } from "ngx-bar-rating";
import { FooterComponent } from './footer/footer.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [MarkAsteriskDirective, RoundPipe, TranslateComponent, FooterComponent, NotFoundComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgxPaginationModule,
    HttpClientModule,
    MaterialModule,
    BarRatingModule,
    RouterModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [LogService, NgxTransService],
  exports: [CommonModule, NgxPaginationModule, FontAwesomeModule, HttpClientModule, MaterialModule, MarkAsteriskDirective, RoundPipe,
    TranslateModule, TranslateComponent, FooterComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {

  constructor(library: FaIconLibrary) {
    library.addIcons(faFacebook, faTwitter, faGoogle, faTachometerAlt, faClock, faUser, faAddressBook, faPowerOff, faSearch, faShoppingBag,
      faAngleRight, faAngleDown, faBars, faRocket, faSync, faLock, faTag, faMinusSquare, faPhone, faEnvelope, faLocationArrow, faStar, faBan,
      faCheckSquare, faSpinner, faExclamationTriangle, faQuestion, faTruck, faBox, faTimes, faFilePdf, faFilter, faArrowCircleUp, faArrowCircleDown,
      faChartPie, faProductHunt, faWindowClose, faChartBar, faTags, faEdit, faTrash);
  }

}
