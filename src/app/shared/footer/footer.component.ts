import { Component, Input, OnInit } from '@angular/core';
import { DescriptionResource } from 'src/app/products/model/products.model';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { FooterConfig } from './footer.config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  serviceItems: DescriptionResource[] = [];
  footerCfg: FooterConfig;

  constructor(private translateService: NgxTransService) {
    this.footerCfg = new FooterConfig(translateService);
  }

  ngOnInit(): void {
    this.footerCfg.footerResource.subscribe((currentItem: DescriptionResource) => {
      this.serviceItems.push(currentItem);
    });
  }
}
