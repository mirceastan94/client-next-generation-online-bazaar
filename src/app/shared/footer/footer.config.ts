import { ReplaySubject } from "rxjs";
import { DescriptionResource } from "src/app/products/model/products.model";
import { NgxTransService } from "src/app/shared/translate/translate.service";

export class FooterConfig {

  public footerResource = new ReplaySubject<DescriptionResource>();
  public footerTranslations: any[] = [];
  private readonly MISCELLANEOUS_LABEL = "miscellaneous";
  private readonly FEATURES_LABEL = this.MISCELLANEOUS_LABEL.concat(".features");

  private readonly footerMap = new Map()
  .set("free-shipping", { uri: "./dashboard", icon: "rocket" })
  .set("free-return", { uri: "../profile", icon: "sync" })
  .set("secure-payment", { uri: "../about", icon: "lock" })
  .set("best-price", { uri: "N/A", icon: "tag" });

  constructor(private translateService: NgxTransService) {
    this.getFooterModel();
  }

  public getFooterModel() {
    this.translateService.getKeyTranslation(this.FEATURES_LABEL).subscribe((translations) => {
      this.footerTranslations = translations;
      Object.keys(this.footerTranslations).forEach((currentTranslation: any) => {
        this.footerResource.next({
          name: this.footerTranslations[currentTranslation]["label"],
          description: this.footerTranslations[currentTranslation]["description"],
          icon: this.footerMap.get(currentTranslation) ? this.footerMap.get(currentTranslation).icon : "N/A"
        });
      });
    });
  }

}
