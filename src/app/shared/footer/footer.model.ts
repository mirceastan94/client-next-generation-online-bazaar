import { IconName } from "@fortawesome/fontawesome-svg-core";

export class DescriptionResource {
  name: string;
  description: string;
  icon: IconName;
}
