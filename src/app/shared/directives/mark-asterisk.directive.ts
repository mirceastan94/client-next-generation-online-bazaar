import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[required]'
})
export class MarkAsteriskDirective implements OnInit {

  constructor(private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    const parentNode = this.renderer.parentNode(this.el.nativeElement);
    if (parentNode.getElementsByTagName('LABEL').length && !parentNode.getElementsByClassName('required-asterisk').length) {
      parentNode.getElementsByTagName('LABEL')[0].innerHTML += '<span class="required-asterisk">*</span>';
    }
  }

}
