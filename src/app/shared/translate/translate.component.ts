import { Component, Input, OnInit } from '@angular/core';
import { NgxTransService } from 'src/app/shared/translate/translate.service';

@Component({
  selector: 'app-language',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss']
})
export class TranslateComponent implements OnInit {

  @Input()
  alignClass: string;

  constructor(private translateService: NgxTransService) { }

  ngOnInit(): void {
  }


  changeLanguage(langValue: string) {
    this.translateService.applyLanguage(langValue);
  }

}
