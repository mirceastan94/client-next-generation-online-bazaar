import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

const localeRegEx = /en|de|ro/;
const ENGLISH_LOC = 'en';
const GERMAN_LOC = 'de';
const ROMANIAN_LOC = 'ro';
const APP_TITLE = 'app-title';

@Injectable({ providedIn: 'root' })
export class NgxTransService {

  langMap = this.initLangLabels();

  /**
  * This constructor initializes the translate services, along the title one,
  * to modify the title value based on the selected language
  * @param translateService
  * @param titleService
  */
  constructor(private translateService: TranslateService, private titleService: Title) {
    const browserLang = ENGLISH_LOC;
    translateService.addLangs([ENGLISH_LOC, GERMAN_LOC, ROMANIAN_LOC]);
    translateService.setDefaultLang(ENGLISH_LOC);
    translateService.use(browserLang.match(localeRegEx) ? browserLang : ENGLISH_LOC);
  }

  /**
   * Applies the user's selected language, including the title
   * @param langLabel
   */
  public applyLanguage(langLabel: string) {
    this.translateService.use(this.langMap.get(langLabel));
    this.translateService.get(APP_TITLE).subscribe((translatedTitle: string) => {
      this.titleService.setTitle(translatedTitle);
    });
  }

  public getMessageTranslation(message: string) {
    const currentLang = this.translateService.currentLang;
    const returnValue = this.translateService.translations[currentLang][message];
    if (returnValue === undefined) {
      return this.translateService.translations.en_merch[message];
    } else {
      return returnValue;
    }
  }

  public getKeyTranslation(key: string) {
    return this.translateService.get(key);
  };

  public getCurrentLanguage() {
    return this.translateService.currentLang;
  };

  public onLanguageChange(): Observable<any> {
    return this.translateService.onLangChange;
  }

  public onDefaultLanguageChange(): Observable<any> {
    return this.translateService.onDefaultLangChange;
  }

  public getInstant(key: string): string {
    return this.translateService.instant(key);
  }

  /**
   * This method initializes the languages locales
   */
  private initLangLabels() {
    const languagesMap = new Map();

    languagesMap.set('English', ENGLISH_LOC);
    languagesMap.set('Englisch', ENGLISH_LOC);
    languagesMap.set('Engleză', ENGLISH_LOC);

    languagesMap.set('German', GERMAN_LOC);
    languagesMap.set('Deutsch', GERMAN_LOC);
    languagesMap.set('Germană', GERMAN_LOC);

    languagesMap.set('Romanian', ROMANIAN_LOC);
    languagesMap.set('Rumänisch', ROMANIAN_LOC);
    languagesMap.set('Română', ROMANIAN_LOC);

    return languagesMap;
  }
}
