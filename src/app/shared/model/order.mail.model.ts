export class EmailDetails {

  orderId: string;
  action: string;

  constructor(orderId: string, action: string) {
    this.orderId = orderId;
    this.action = action;
  }

}
