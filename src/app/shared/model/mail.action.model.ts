export enum OrderMailAction {
  Confirmation = "CONFIRMATION",
  Cancellation = "CANCELLATION"
}
