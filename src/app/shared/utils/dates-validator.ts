import { AbstractControl, ValidatorFn } from "@angular/forms";

export class DatesValidator {

  static datesDelta = (fromDate: string, toDate: string): ValidatorFn => {
    return (control: AbstractControl) => {
      const from = control.get(fromDate);
      const to = control.get(toDate);

      if (from === null || to === null) {
        return null;
      }

      if (from.value > to.value) {
        return { notValid: true };
      } else {
        return null;
      }
    };
  }

}
