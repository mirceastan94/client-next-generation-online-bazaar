import { AbstractControl, ValidatorFn } from '@angular/forms';

export class PasswordValidator {
  static passwordsMatch = (controlName: string, matchingControlName: string): ValidatorFn => {
    return (control: AbstractControl) => {
      const input = control.get(controlName);
      const matchingInput = control.get(matchingControlName);

      if (input === null || matchingInput === null) {
        return null;
      }

      if (matchingInput?.errors && !matchingInput.errors.mustMatch) {
        return null;
      }

      if (input.value !== matchingInput.value) {
        matchingInput.setErrors({ mustMatch: true });
        return ({ mustMatch: true });
      } else {
        matchingInput.setErrors(null);
        return null;
      }
    };
  }
}
