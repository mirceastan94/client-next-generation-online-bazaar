import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  log(message: any) {
    console.log(new Date() + ": " + JSON.stringify(message));
  }

  error(message: any) {
    console.error(new Date() + ": " + JSON.stringify(message));
  }
}
