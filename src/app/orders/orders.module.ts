import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { StatusComponent } from './status/status.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { OrdersRoutingModule } from './orders-routings.module';
import { ProductsModule } from '../products/products.module';
import { CancelDialogComponent } from './status/cancel-dialog/cancel-dialog.component';

@NgModule({
  declarations: [ListComponent, StatusComponent, CancelDialogComponent],
  imports: [
    CommonModule, OrdersRoutingModule, ProductsModule, SharedModule, RouterModule
  ]
})
export class OrdersModule { }
