import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { OrderMailAction } from 'src/app/shared/model/mail.action.model';
import { MessageResponseData } from '../../model/message.response';
import { OrderCancelStatus } from '../../model/order-cancel-status.model';
import { Order } from '../../model/order.model';
import { OrdersService } from '../../orders.service';

@Component({
  selector: 'app-cancel-dialog',
  templateUrl: './cancel-dialog.component.html',
  styleUrls: ['./cancel-dialog.component.scss'],
})
export class CancelDialogComponent implements OnInit {

  public order: Order;
  public cancellationStatus: OrderCancelStatus = OrderCancelStatus.Confirmation;
  public readonly orderCancelStatus = OrderCancelStatus;

  constructor(private dialogRef: MatDialogRef<CancelDialogComponent>, @Inject(MAT_DIALOG_DATA) public dialogData: { orderId: string },
    private router: Router, private orderService: OrdersService) { }

  ngOnInit(): void {
  }

  initiateOrderCancellation(): void {
    this.cancellationStatus = OrderCancelStatus.InProgress;
    this.orderService.cancelOrder(this.dialogData.orderId).subscribe(
      (cancelStatus: MessageResponseData) => {
        if (cancelStatus.success) {
          this.cancellationStatus = OrderCancelStatus.Successful;
          this.orderService.sendCancellationEmail(this.dialogData.orderId, OrderMailAction.Cancellation).subscribe();
        } else {
          this.cancellationStatus = OrderCancelStatus.Unsuccessful;
        }
      },
      (error) => {
        this.cancellationStatus = OrderCancelStatus.Unsuccessful;
      }
    );
  }

  checkMyOrders(): void {
    this.dialogRef.close();
    this.router.navigate(['/orders/my-orders']);
  }

}
