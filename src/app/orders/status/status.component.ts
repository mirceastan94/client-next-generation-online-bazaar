import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { Order } from '../model/order.model';
import { OrderStatus } from '../model/order.status.model';
import { OrdersService } from '../orders.service';
import { CancelDialogComponent } from './cancel-dialog/cancel-dialog.component';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {

  public order: Order;
  public deliveryDate: string;
  public statusTranslation: string;

  public atLeastConfirmedOffer: boolean = false;
  public atLeastPendingOffer: boolean = false;
  public confirmedOffer: boolean = false;
  public pendingOffer: boolean = false;
  public completedOffer: boolean = false;
  public cancelledOffer: boolean = false;

  private readonly ORDERS_LABEL = "orders.tracking.info.status";
  private readonly DATE_FORMAT = "yyyy-MM-dd";
  private readonly POINT = ".";

  statesMap: Map<string, string> = new Map();

  constructor(private route: ActivatedRoute, private orderService: OrdersService, private translateService: NgxTransService, private dialog: MatDialog) {
    Object.values(OrderStatus).forEach(currentValue => {
      this.statesMap.set(currentValue.toUpperCase(), this.ORDERS_LABEL.concat(this.POINT).concat(currentValue).toLowerCase());
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(routeParams => {
      this.orderService.retrieveOrder(routeParams.id).subscribe((foundOrder: Order[]) => {
        this.order = foundOrder[0];
        this.translateService.getKeyTranslation(this.statesMap.get(this.order.status)).subscribe((translation) => {
          this.statusTranslation = translation;
        });
        const orderDate = new Date(this.order.dateOfPurchase);
        orderDate.setDate(orderDate.getDate() + 2);
        this.deliveryDate = formatDate(orderDate, this.DATE_FORMAT, "en");
        this.computeOrderStates();
      });
    });
  }

  downloadOrderAsPdf(): void {
    const orderData = document.getElementById('orderContent');
    html2canvas(orderData).then(canvas => {
      const docWidth = 290;
      const docHeight = canvas.height * docWidth / canvas.width;
      const orderDataURL = canvas.toDataURL('image/png');
      let jsPdf = new jsPDF('l', 'mm', 'A4');
      jsPdf.addImage(orderDataURL, 'PNG', 3, 3, docWidth, docHeight);
      jsPdf.save('order-'.concat(this.order.id).concat('.pdf'));
    });
  }

  cancelOrder(): void {
    this.dialog.open(CancelDialogComponent, {
      width: '500px',
      position: {
        'top': '25px',
        'left': '50px'
      },
      data: { orderId: this.order.id },
      backdropClass: "backdrop",
      panelClass: "order-dialog-container"
    });
  }

  computeOrderStates(): void {
    const orderStatus = this.order.status;
    this.atLeastConfirmedOffer = [OrderStatus.CONFIRMED.toString(), OrderStatus.PENDING.toString(), OrderStatus.COMPLETED.toString()].indexOf(orderStatus) > -1;
    this.atLeastPendingOffer = [OrderStatus.PENDING.toString(), OrderStatus.COMPLETED.toString()].indexOf(orderStatus) > -1;
    this.confirmedOffer = OrderStatus.CONFIRMED === orderStatus;
    this.pendingOffer = OrderStatus.PENDING === orderStatus;
    this.completedOffer = OrderStatus.COMPLETED === orderStatus;
    this.cancelledOffer = OrderStatus.CANCELLED === orderStatus;
  }

}
