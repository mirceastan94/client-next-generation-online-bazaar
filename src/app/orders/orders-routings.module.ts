import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../auth/auth.guard";
import { StatusComponent } from "./status/status.component";
import { ListComponent } from "./list/list.component";

const appRoutes: Routes = [
  { path: 'my-orders', component: ListComponent, canActivate: [AuthGuard] },
  { path: ':id', component: StatusComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
