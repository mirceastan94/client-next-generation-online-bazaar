import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, delay } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { LogService } from "../shared/logging/log.service";
import { OrderMailAction } from "../shared/model/mail.action.model";
import { EmailDetails } from "../shared/model/order.mail.model";
import { MessageResponseData } from "./model/message.response";
import { Order } from "./model/order.model";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private httpClient: HttpClient, private logger: LogService) { }

  retrieveAllOrders(): Observable<Order[]> {
    return this.httpClient.get<Order[]>(environment.ordersApiUrl.concat("/es/all")).pipe(catchError((this.handleError)));
  }

  retrieveOrder(id: string): Observable<Order[]> {
    return this.httpClient.get<Order[]>(environment.ordersApiUrl.concat("/es/id/").concat(id)).pipe(catchError((this.handleError)));
  }

  cancelOrder(id: string): Observable<MessageResponseData> {
    return this.httpClient.delete<MessageResponseData>(environment.ordersApiUrl.concat("/cancel/").concat(id)).pipe(delay(1000), catchError((this.handleError)));
  }

  retrieveOrdersForUser(userId: string): Observable<Order[]> {
    return this.httpClient.get<Order[]>(environment.ordersApiUrl.concat("/list/").concat(userId)).pipe(catchError((this.handleError)));
  }

  sendCancellationEmail(orderId: string, orderMailAction: OrderMailAction): Observable<{ success: boolean, message: string }> {
    return this.httpClient.post<{ success: boolean, message: string }>(environment.ordersApiUrl.concat("/sendEmail"),
    new EmailDetails(orderId, orderMailAction))
    .pipe(catchError(this.handleError));
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = 'unknown-error';
    if (!errorRes.error || !errorRes.error.message) {
      return throwError(errorMessage);
    }
    console.error("Response error: " + JSON.stringify(errorRes.error));
    return throwError(errorRes.error.message);
  }

}
