export class Order {
  id: string;
  status: string;
  translatedState: string;
  price: number;
  dateOfPurchase: string;
  cartDto: Cart;
  userDto: User;

  constructor(id: string, status: string, price: number, dateOfPurchase: string, cartDto: Cart, userDto: User) {
    this.id = id;
    this.status = status;
    this.price = price;
    this.dateOfPurchase = dateOfPurchase;
    this.cartDto = cartDto;
    this.userDto = userDto;
  }
}

export class Cart {
  cartProductDtos: Product[];

  constructor(cartProductDtos: Product[]) {
    this.cartProductDtos = cartProductDtos;
  }
}

export class Product {
  productDto: ProductDetails;
  quantity: number;
  totalPrice: number;
}

export class ProductDetails {
  id: string;
  name: string;
  description: string;
  category: string;
  subCategory: string;
  stockCount: number;
  brand: string;
  price: number;
  discountPrice: number;
  rating: number;
  imageData: string;
}

export class User {
  id: string;
  name: string;
  email: string;
  address: string;

  constructor(id: string, name: string, email: string, address: string) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.address = address;
  }
}
