export enum OrderCancelStatus {
  Confirmation = "CONFIRMATION",
  InProgress = "IN_PROGRESS",
  Successful = "OUT_OF_STOCK_PRODUCTS",
  Unsuccessful = "UNSUCCESSFUL_PAYMENT"
}
