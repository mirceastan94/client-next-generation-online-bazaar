import { Component, OnDestroy, OnInit } from '@angular/core';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { Subscription } from 'rxjs';
import { Translation } from 'src/app/products/model/products.model';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { Order, User } from '../model/order.model';
import { OrdersService } from '../orders.service';

@Component({
  selector: 'my-orders',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  public userOrders: Order[] = [];
  public filteredOrders: Order[] = [];
  public ordersSub$: Subscription;

  public statesTranslations: Translation[] = [];
  public loggedUser: { accessToken: string, expiryDate: Date, tokenType: string, userDto: User };

  public readonly USER_DATA_LABEL: string = 'userData';
  public readonly ORDERS_TRACKING_INFO_LABEL: string = 'orders.tracking.info.status';
  public readonly PRICE: string = 'price';
  public readonly DATE: string = 'date';
  public readonly ASCENDING: string = 'ascending';
  public readonly DESCENDING: string = 'descending';

  constructor(private orderService: OrdersService, private translateService: NgxTransService) { }

  ngOnInit(): void {
    this.loggedUser = localStorage.getItem(this.USER_DATA_LABEL) ? JSON.parse(localStorage.getItem(this.USER_DATA_LABEL)) : JSON.parse(sessionStorage.getItem(this.USER_DATA_LABEL));
    this.ordersSub$ = this.orderService.retrieveOrdersForUser(this.loggedUser.userDto.id).subscribe((retrievedOrders) => {
      this.userOrders = retrievedOrders;
      this.filteredOrders = this.userOrders;
      this.userOrders.forEach(order => {
        order.translatedState = this.statesTranslations.find(status => status.key === order.status.toLowerCase()).label;
      });
    });
    this.translateService.getKeyTranslation(this.ORDERS_TRACKING_INFO_LABEL).subscribe((translations) => {
      Object.entries(translations).forEach(([key, value]) => {
        if (key.toString().toLowerCase() !== 'label') {
          this.statesTranslations.push(new Translation(key, value.toString()));
        }
      })
    });
  }

  downloadUserOrdersAsPdf(): void {
    const orderData = document.getElementById('ordersTable');
    html2canvas(orderData).then(canvas => {
      const docWidth = 290;
      const docHeight = canvas.height * docWidth / canvas.width;
      const orderDataURL = canvas.toDataURL('image/png');
      let jsPdf = new jsPDF('l', 'mm', 'A4');
      jsPdf.addImage(orderDataURL, 'PNG', 3, 3, docWidth, docHeight);
      jsPdf.save('orders-'.concat(this.loggedUser.userDto.id).concat('.pdf'));
    });
  }

  refreshPage(): void {
    location.reload();
  }

  onChangeOrderStatus(orderStatus: any): void {
    if (orderStatus.target.value == 'Any') {
      this.filteredOrders = this.userOrders;
    } else {
      const statusKey = this.statesTranslations.find(stateTrans => stateTrans.label === orderStatus.target.value).key;
      this.filteredOrders = this.userOrders.filter(order => order.status.toLowerCase() === statusKey);
    }
  }

  onSortBy(criteria: string, order: string): void {
    if (criteria === this.PRICE) {
      if (order === this.ASCENDING) {
        this.filteredOrders = this.userOrders.sort((firstOrder, secondOrder) => firstOrder.price < secondOrder.price ? -1 : 1);
      } else {
        this.filteredOrders = this.userOrders.sort((firstOrder, secondOrder) => firstOrder.price > secondOrder.price ? -1 : 1);
      }
    }
    if (criteria === this.DATE) {
      if (order === this.ASCENDING) {
        this.filteredOrders = this.userOrders.sort((firstOrder, secondOrder) => new Date(firstOrder.dateOfPurchase) < new Date(secondOrder.dateOfPurchase) ? -1 : 1);
      } else {
        this.filteredOrders = this.userOrders.sort((firstOrder, secondOrder) => new Date(firstOrder.dateOfPurchase) > new Date(secondOrder.dateOfPurchase) ? -1 : 1);
      }
    }
  }

  ngOnDestroy(): void {
    this.ordersSub$.unsubscribe();
  }


}
