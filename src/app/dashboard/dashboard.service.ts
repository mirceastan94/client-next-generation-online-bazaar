import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { CampaignDto } from "./model/campaign.model";
import { MessageResponseData } from "./model/message.response";
import { Order, ProductDetails } from "./model/order.model";
import { Criterion, ProductCategory } from "./model/product-category.model";
import { Review } from "./model/review.model";
import { User, UserRoleDto } from "./model/user.model";

@Injectable({ providedIn: 'root' })
export class DashboardService {

  constructor(private httpClient: HttpClient) {
  }

  getProductsCount(): Observable<number> {
    return this.httpClient.get<number>(environment.productsApiUrl.concat("/count")).pipe(catchError(this.handleError));
  }

  getOrdersCount(): Observable<number> {
    return this.httpClient.get<number>(environment.ordersApiUrl.concat("/count")).pipe(catchError(this.handleError));
  }

  getUsersCount(): Observable<number> {
    return this.httpClient.get<number>(environment.productsApiUrl.concat("/user/count")).pipe(catchError(this.handleError));
  }

  getRecentOrders(): Observable<Order[]> {
    return this.httpClient.get<Order[]>(environment.ordersApiUrl.concat("/list/recent")).pipe(catchError(this.handleError));
  }

  getRecentReviews(): Observable<Review[]> {
    return this.httpClient.get<Review[]>(environment.productsApiUrl.concat("/review/recent")).pipe(catchError(this.handleError));
  }

  getProductsCategoriesWithCount(): Observable<ProductCategory[]> {
    return this.httpClient.get<ProductCategory[]>(environment.productsApiUrl.concat("/categories-with-count")).pipe(catchError(this.handleError));
  }

  getCampaignsList(): Observable<CampaignDto[]> {
    return this.httpClient.get<CampaignDto[]>(environment.productsApiUrl.concat("/campaign/find/all")).pipe(catchError(this.handleError));
  }

  getCampaign(id: string): Observable<CampaignDto> {
    return this.httpClient.get<CampaignDto>(environment.productsApiUrl.concat("/campaign/find/").concat(id)).pipe(catchError(this.handleError));
  }

  createOrUpdateCampaign(campaign: CampaignDto): Observable<MessageResponseData> {
    return this.httpClient.post<MessageResponseData>(environment.productsApiUrl.concat("/campaign/create-or-update"), campaign)
      .pipe(catchError(this.handleError));
  }

  deleteCampaign(id: string): Observable<MessageResponseData> {
    return this.httpClient.get<MessageResponseData>(environment.productsApiUrl.concat("/campaign/delete/").concat(id)).pipe(catchError(this.handleError));
  }

  retrieveProducts(criterion: Criterion): Observable<ProductDetails[]> {
    return this.httpClient.post<ProductDetails[]>(environment.productsApiUrl.concat("/find-by-criteria"), criterion).pipe(catchError((this.handleError)));
  }

  getUsersListWithCriteria(criterion: Criterion): Observable<User[]> {
    return this.httpClient.post<User[]>(environment.productsApiUrl.concat("/user/find-by-criteria"), criterion).pipe(catchError(this.handleError));
  }

  getUsersCountWithCriteria(criterion: Criterion): Observable<number> {
    return this.httpClient.post<number>(environment.productsApiUrl.concat("/user/count-by-criteria"), criterion).pipe(catchError(this.handleError));
  }

  updateUserRole(userId: string, roleName: string): Observable<MessageResponseData> {
    const userRoleDto = new UserRoleDto(userId, roleName);
    return this.httpClient.post<MessageResponseData>(environment.productsApiUrl.concat("/user/update-role"), userRoleDto).pipe(catchError(this.handleError));
  }

  deleteUser(userId: string): Observable<MessageResponseData> {
    return this.httpClient.get<MessageResponseData>(environment.productsApiUrl.concat("/user/delete/").concat(userId)).pipe(catchError(this.handleError));
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = 'unknown-error';
    if (!errorRes.error || !errorRes.error.message) {
      return throwError(errorMessage);
    }
    console.error("Response error: " + JSON.stringify(errorRes.error));
    return throwError(errorRes.error.message);
  }

}
