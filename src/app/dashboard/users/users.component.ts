import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ColDef, FirstDataRenderedEvent, GridApi, GridOptions, HeaderValueGetterParams, ICellRendererParams } from 'ag-grid-community';
import { forkJoin } from 'rxjs';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { DashboardService } from '../dashboard.service';
import { Translation } from '../model/misc.model';
import { Criterion } from '../model/product-category.model';
import { User } from '../model/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public gridOptions: GridOptions;
  public gridApi: GridApi;
  public gridState: { pageSize: number };
  public gridPageSizes: number[];
  public searchCriterion: Criterion = {
    filterCriteria: {
      "isDeleted": "false"
    },
    page: 0,
    size: 5,
    sortByField: "name",
    order: "ASC"
  };
  public rolesTranslation: Translation[] = [];
  public userDeletionTranslations: Translation[] = [];
  public searchedUsers: User[] = [];
  public isRowSelected: boolean = false;

  public readonly ROLES_LABEL: string = "dashboard.users.options.change-role";
  public readonly DELETE_USER_LABEL: string = "dashboard.users.options.delete-user";
  public readonly USER_ROLE: string = "ROLE_USER";
  public readonly ADMIN_ROLE: string = "ROLE_ADMIN";
  public readonly YES: string = "yes";
  public readonly NO: string = "no";
  public readonly ID: string = "id";

  constructor(private dashboardService: DashboardService, private translateService: NgxTransService) {
    this.gridPageSizes = [5, 10, 15, 20, 25];
    this.gridState = { pageSize: this.gridPageSizes[0] };
    this.translateService.getKeyTranslation(this.ROLES_LABEL).subscribe((rolesTrans) => {
      this.rolesTranslation.push(new Translation(this.USER_ROLE, rolesTrans['user']));
      this.rolesTranslation.push(new Translation(this.ADMIN_ROLE, rolesTrans['admin']));
    });
    this.translateService.getKeyTranslation(this.DELETE_USER_LABEL).subscribe((userDelTrans) => {
      this.userDeletionTranslations.push(new Translation(this.YES, userDelTrans[this.YES]));
      this.userDeletionTranslations.push(new Translation(this.NO, userDelTrans[this.NO]));
    });
  }

  ngOnInit(): void {
    const columnDefs: ColDef[] = [
      {
        headerName: 'dashboard.users.headers.role.label', field: 'role', sortable: true, filter: true, resizable: true, minWidth: 120,
        headerValueGetter: (field) => { return this.localizeHeader(field) }, headerClass: 'header-class', editable: false
      },
      {
        headerName: 'dashboard.users.headers.name', field: 'name', sortable: true, filter: true, resizable: true, pinned: 'left', minWidth: 120,
        headerValueGetter: (field) => { return this.localizeHeader(field) }, headerClass: 'header-class', editable: false
      },
      {
        headerName: 'dashboard.users.headers.email', field: 'email', sortable: true, filter: true, resizable: true, minWidth: 120,
        headerValueGetter: (field) => { return this.localizeHeader(field) }, headerClass: 'header-class', editable: false
      }
    ];

    this.gridOptions = {
      columnDefs: columnDefs,
      pagination: true,
      api: this.gridApi,
      paginationPageSize: this.gridState.pageSize,
      rowHeight: 50,
      headerHeight: 60,
      cacheBlockSize: this.gridState.pageSize,
      maxBlocksInCache: 1,
      domLayout: 'autoHeight',
      rowSelection: 'single',
      rowModelType: 'infinite',
      onCellClicked: (event: any) => { this.onCellClicked(event) },
      onPaginationChanged: (event: any) => { this.onPaginationChanged(event); }
    };
    this.searchCriterion.size = this.gridState.pageSize;
  }

  public onFirstDataRendered(event: FirstDataRenderedEvent) {
    event.api.sizeColumnsToFit();
  }

  public onGridReady(parameters: any): void {
    this.gridApi = parameters.api;
    this.translateService.onLanguageChange().subscribe(() => {
      this.gridApi.refreshHeader();
    });
    this.translateService.onDefaultLanguageChange().subscribe(() => {
      this.gridApi.refreshHeader();
    });
    this.generateDataSource();
  }

  public generateDataSource(): void {
    const dataSource = {
      getRows: (params: any) => {
        if (params.sortModel[0]) {
          this.searchCriterion['sortByField'] = params.sortModel[0].colId;
          this.searchCriterion['order'] = params.sortModel[0].sort.toUpperCase();
        }
        forkJoin([
          this.dashboardService.getUsersListWithCriteria(this.searchCriterion),
          this.dashboardService.getUsersCountWithCriteria(this.searchCriterion)
        ]).subscribe(
          (usersData: any[]) => {
            this.searchedUsers = usersData[0];
            if (this.searchedUsers.length > 0) {
              this.searchedUsers.forEach(currentUser => {
                currentUser.role = this.rolesTranslation.find(roleTrans => roleTrans.key === currentUser.role).label.toUpperCase();
              });
              params.successCallback(usersData[0], usersData[1]);
            } else {
              this.gridApi.showNoRowsOverlay();
            }
          },
          (error) => {
            this.gridApi.showNoRowsOverlay();
          });
      }
    }
    if (this.gridApi) {
      this.gridApi.setDatasource(dataSource);
    }
  }

  onCellClicked(event: any) {
    this.isRowSelected = !this.isRowSelected;
  }

  public onPaginationChanged(event: any): void {
    if (event['newPage']) {
      this.searchCriterion['page'] = event['api']['paginationProxy']['currentPage'];
    }
  }

  public localizeHeader(parameters: HeaderValueGetterParams): any {
    const headerIdentifier = parameters.colDef.headerName;
    return this.translateService.getInstant(headerIdentifier);
  }

  public changeGridPageSize(size: number) {
    this.gridOptions.api.paginationSetPageSize(size);
    this.gridState.pageSize = size;
    this.searchCriterion.size = this.gridState.pageSize;
    this.generateDataSource();
    this.isRowSelected = false;
  }

  public changeUserRole(roleKey: string) {
    const selectedUser = this.getSelectedRows();
    this.dashboardService.updateUserRole(selectedUser[0][this.ID], roleKey).subscribe(() => {
      this.generateDataSource();
      this.isRowSelected = false;
    });
  }

  public deleteSelectedUser(deletionDecision: string) {
    if (deletionDecision === this.YES) {
      const selectedUser = this.getSelectedRows();
      this.dashboardService.deleteUser(selectedUser[0][this.ID]).subscribe(() => {
        this.generateDataSource();
        this.isRowSelected = false;
      });
    }
  }

  public getSelectedRows() {
    return this.gridOptions.api.getSelectedRows();
  }

}
