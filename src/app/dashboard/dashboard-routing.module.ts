import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from '../auth/admin.guard';
import { DashboardComponent } from './dashboard.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'categories/statistics', canActivate: [AdminGuard]
  },
  {
    path: 'categories', component: DashboardComponent, canActivate: [AdminGuard], children: [
      { path: 'statistics', component: StatisticsComponent, canActivate: [AdminGuard] },
      { path: 'campaigns', component: CampaignsComponent, canActivate: [AdminGuard] },
      { path: 'users', component: UsersComponent, canActivate: [AdminGuard] }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {

}
