import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { DashboardService } from '../dashboard.service';
import { MainChart } from '../model/charts.model';
import { Translation } from '../model/misc.model';
import { Order } from '../model/order.model';
import { ProductCategory } from '../model/product-category.model';
import { Review } from '../model/review.model';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit, OnDestroy {

  public mainChartsData: MainChart[] = [];
  public productsCategoriesData: ProductCategory[] = [];
  public colorScheme: any;

  public ordersObs: Observable<Order[]>;
  public reviewsObs: Observable<Review[]>;

  private orderSub$: Subscription;
  private productsSub$: Subscription;
  private usersSub$: Subscription;

  private generalStatsTrans: Translation[] = [];

  private DASHBOARD_STATISTICS_TRANS_LABEL = "dashboard.statistics.";
  private STATISTICS_TRANS_GENERAL_LABEL = "general";

  constructor(private dashboardService: DashboardService, private translateService: NgxTransService) { }

  ngOnInit(): void {
    this.translateService.getKeyTranslation(this.DASHBOARD_STATISTICS_TRANS_LABEL.concat(this.STATISTICS_TRANS_GENERAL_LABEL)).subscribe((generalStatsTrans) => {
      Object.entries(generalStatsTrans).forEach(([key, value]) => this.generalStatsTrans.push(new Translation(key.toString(), value.toString())));
    });

    this.orderSub$ = this.dashboardService.getOrdersCount().subscribe((ordersCount) => {
      const orderTrans = this.generalStatsTrans.find(currentTrans => currentTrans.key === 'orders-count');
      this.mainChartsData.push(new MainChart(orderTrans.label, ordersCount));
    });
    this.productsSub$ = this.dashboardService.getProductsCount().subscribe((productsCount) => {
      const productTrans = this.generalStatsTrans.find(currentTrans => currentTrans.key === 'products-count');
      this.mainChartsData.push(new MainChart(productTrans.label, productsCount));
    });
    this.usersSub$ = this.dashboardService.getUsersCount().subscribe((usersCount) => {
      const userTrans = this.generalStatsTrans.find(currentTrans => currentTrans.key === 'users-count');
      this.mainChartsData.push(new MainChart(userTrans.label, usersCount));
    });

    this.dashboardService.getProductsCategoriesWithCount().subscribe((categories) => {
      this.productsCategoriesData = categories;
    });

    this.ordersObs = this.dashboardService.getRecentOrders();
    this.reviewsObs = this.dashboardService.getRecentReviews();

    this.colorScheme = {
      domain: [
        'rgb(255, 166, 0)',
        '#222'
      ]
    };
  }

  ngOnDestroy(): void {
    this.orderSub$.unsubscribe();
    this.productsSub$.unsubscribe();
    this.usersSub$.unsubscribe();
  }

}
