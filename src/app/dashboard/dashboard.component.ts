import { Component, OnInit } from '@angular/core';
import { NgxTransService } from '../shared/translate/translate.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private translateService: NgxTransService) { }

  ngOnInit(): void {
  }

}
