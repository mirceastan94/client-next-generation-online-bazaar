import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, timer } from 'rxjs';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { DashboardService } from '../dashboard.service';
import { CampaignDto } from '../model/campaign.model';
import { CreateOrEditDialogComponent } from './create-or-edit-dialog/create-or-edit-dialog.component';

@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.scss']
})
export class CampaignsComponent implements OnInit {

  public campaignArray: CampaignDto[] = [];
  public isActive: boolean = true;
  public campaigns$: Subscription;
  public readonly STATES_LABEL: string = "dashboard.campaigns.status";

  constructor(private dashboardService: DashboardService, private createDialog: MatDialog, private translateService: NgxTransService) { }

  ngOnInit(): void {
    this.campaigns$ = timer(0, 10000).subscribe(() => {
      this.dashboardService.getCampaignsList().subscribe((campaigns) => {
        this.campaignArray = campaigns;
        this.translateService.getKeyTranslation(this.STATES_LABEL).subscribe((statesTranslations) => {
          this.campaignArray.forEach((campaign) => {
            const statusTrans = Object.entries(statesTranslations).find(([key]) => key === campaign.status)[1].toString();
            campaign['statusLabel'] = statusTrans;
          })
        });
      });

    });
  }

  createCampaign(campaignId?: string, campaignStatus?: string): void {
    this.createDialog.open(CreateOrEditDialogComponent, {
      width: '675px',
      height: '700px',
      position: {
        'top': '20px'
      },
      data: { campaignId, campaignStatus },
      autoFocus: false,
      backdropClass: 'backdrop'
    });
  }

  editCampaign(campaign: CampaignDto): void {
    this.createCampaign(campaign.id, campaign.statusLabel);
  }

  removeCampaign(campaignId: string): void {
    this.dashboardService.deleteCampaign(campaignId).subscribe(() => {
      this.dashboardService.getCampaignsList().subscribe((campaigns) => {
        this.campaignArray = campaigns;
      })
    });
  }

  ngOnDestroy() {
    this.campaigns$.unsubscribe();
  }

}
