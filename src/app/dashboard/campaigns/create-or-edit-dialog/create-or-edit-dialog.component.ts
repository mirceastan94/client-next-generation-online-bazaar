import { Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged, filter, flatMap } from "rxjs/operators";
import { NgxTransService } from "src/app/shared/translate/translate.service";
import { DatesValidator } from "src/app/shared/utils/dates-validator";
import { DashboardService } from "../../dashboard.service";
import { CampaignDto, CampaignProcessingStatus, CampaignStatus } from "../../model/campaign.model";
import { Translation } from "../../model/misc.model";
import { ProductDetails } from "../../model/order.model";

@Component({
  selector: 'app-create-or-edit-dialog',
  templateUrl: './create-or-edit-dialog.component.html',
  styleUrls: ['./create-or-edit-dialog.component.scss']
})
export class CreateOrEditDialogComponent implements OnInit, OnDestroy {

  public campaign: CampaignDto = new CampaignDto();
  public campaignForm: FormGroup;
  public currentProcessingStep: number = CampaignProcessingStatus.MAIN;
  public states: Translation[] = [];
  public defaultState: Translation;
  public readonly STATES_LABEL: string = "dashboard.campaigns.status";
  public eventSubject = new Subject<string>();
  public searchedProducts: ProductDetails[] = [];
  public campaignProducts: ProductDetails[] = [];

  constructor(private dialogRef: MatDialogRef<CreateOrEditDialogComponent>, @Inject(MAT_DIALOG_DATA) public dialogData: { campaignId: string, campaignStatus: string },
    private dashboardService: DashboardService, private formBuilder: FormBuilder, private translateService: NgxTransService) {
  }

  ngOnInit(): void {
    if (this.dialogData.campaignId) {
      this.dashboardService.getCampaign(this.dialogData.campaignId).subscribe((existingCampaign) => {
        this.campaign = existingCampaign;
        this.campaign['statusLabel'] = this.dialogData.campaignStatus;
        this.campaignProducts = existingCampaign.campaignProducts;
        this.generateCampaignFormGroup();
        this.translateStates();
      });
    } else {
      this.generateCampaignFormGroup();
      this.translateStates();
    }

    this.eventSubject.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      filter(searchedText => searchedText.length > 2),
      flatMap(searchedText =>
        this.dashboardService.retrieveProducts({
          filterCriteria: { "name": searchedText },
          page: 0,
          size: 3,
          sortByField: "id",
          order: "ASC"
        })
      )).subscribe(foundProducts => this.searchedProducts = foundProducts);
  }

  private generateCampaignFormGroup(): void {
    this.campaignForm = this.formBuilder.group({
      name: [this.campaign.name, Validators.required],
      startDate: [this.campaign.startDate, Validators.required],
      endDate: [this.campaign.endDate, Validators.required],
      status: [this.campaign.status, Validators.required]
    });
    this.campaignForm.setValidators(DatesValidator.datesDelta("startDate", "endDate"));
  }

  searchProducts(event: any): void {
    if (!event.target.value) {
      this.searchedProducts = null;
    }
    this.eventSubject.next(event.target.value);
  }

  onNextStep() {
    if (this.campaignForm.valid && this.currentProcessingStep == CampaignProcessingStatus.MAIN) {
      this.currentProcessingStep++;
    }
  }

  onPreviousStep() {
    this.currentProcessingStep--;
  }

  onSubmit(): void {
    if (this.campaignForm.valid) {
      this.assignFormDataToCampaign(this.campaign);
      this.dashboardService.createOrUpdateCampaign(this.campaign).subscribe(
        (apiResponse) => {
          if (apiResponse.success) {
            this.currentProcessingStep = CampaignProcessingStatus.SUCCESSFUL;
          } else {
            this.currentProcessingStep = CampaignProcessingStatus.FAILED;
          }
        },
        (error) => {
          this.currentProcessingStep = CampaignProcessingStatus.FAILED;
        }
      )
    }
  }

  private translateStates() {
    this.translateService.getKeyTranslation(this.STATES_LABEL).subscribe((statesTrans) => {
      Object.entries(statesTrans).forEach(([key, value]) => {
        if (key !== 'label') {
          this.states.push(new Translation(key.toString(), value.toString()));
        }
        if (this.campaign.status) {
          this.campaignForm.controls.status.patchValue(this.campaign.status);
        } else if (key === 'active') {
          this.campaignForm.controls.status.patchValue(key);
        }
      });
    });
  }

  private assignFormDataToCampaign(campaign: CampaignDto) {
    const campaignFormValues = this.campaignForm.value;
    campaign.name = campaignFormValues.name;
    campaign.startDate = campaignFormValues.startDate;
    campaign.endDate = campaignFormValues.endDate;
    campaign.status = campaignFormValues.status;
    campaign.campaignProducts = this.campaignProducts;
  }

  public addProductToList(product: ProductDetails) {
    if (!this.campaignProducts.find((existingProduct) => existingProduct.id == product.id)) {
      this.campaignProducts.push(product);
      this.searchedProducts = this.searchedProducts.filter((currentProduct) => currentProduct.id !== product.id);
    }
  }

  public removeProductFromList(product: ProductDetails) {
    this.campaignProducts = this.campaignProducts.filter((currentProduct) => currentProduct.id !== product.id);
  }

  public changeStatus(event: any): void {
    const selectedStateKey = this.states.find(state => state.key === event.target.value.toString().split(":")[1].trim()).key;
    this.campaignForm.controls.status.setValue(selectedStateKey);
  }

  public changeProductPrice(product: ProductDetails, event: any) {
    product.discountPrice = event.target.value;
    product.priceReduction = (product.price - product.discountPrice);
    const productToUpdateIdx = this.campaignProducts.findIndex(campaignProduct => campaignProduct.id === product.id);
    this.campaignProducts[productToUpdateIdx] = product;
  }

  public closePopup() {
    this.dialogRef.close();
  }

  public get CampaignProcessingStatus() {
    return CampaignProcessingStatus;
  }

  public get form() {
    return this.campaignForm.controls;
  }

  ngOnDestroy() {
    this.eventSubject.unsubscribe();
  }

}
