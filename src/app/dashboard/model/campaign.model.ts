import { ProductDetails } from "./order.model";

export class CampaignDto {
  id: string;
  name: string;
  startDate: string;
  endDate: string;
  status: CampaignStatus;
  statusLabel: string;
  campaignProducts: ProductDetails[];
}

export enum CampaignStatus {
  ACTIVE = "ACTIVE",
  INACTIVE = "INACTIVE"
}

export enum CampaignProcessingStatus {
  MAIN = 1,
  PRODUCTS = 2,
  PENDING = 3,
  SUCCESSFUL = 4,
  FAILED = 5
}
