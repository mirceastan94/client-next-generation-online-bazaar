export class ProductCategory {

  name: string;
  value: number;

  constructor(name: string, value: number) {
    this.name = name;
    this.value = value;
  }

}

export class Criterion {
  name?: string;
  filterCriteria?: any;
  sortOperator?: "equal" | "greaterOrEqual";
  page: number;
  size: number;
  sortByField: string;
  order: string;
}
