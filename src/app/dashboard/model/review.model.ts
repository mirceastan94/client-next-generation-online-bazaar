export class Review {
  id: string;
  content: string;
  dateOfPosting: string;
  rating: number;
  verifiedPurchase: boolean;
  userDto: User;
  productId: string;
  productName: string;

  constructor(content: string, dateOfPosting: string, rating: number, verifiedPurchase: boolean, userDto: User, productId: string, productName: string) {
    this.content = content;
    this.dateOfPosting = dateOfPosting;
    this.rating = rating;
    this.verifiedPurchase = verifiedPurchase;
    this.userDto = userDto;
    this.productId = productId;
    this.productName = productName;
  }
}

export class User {
  constructor(public id: string, public name: string, public email: string, public role: string, public userDetailsDto: UserDetails) {
  }
}

export class UserDetails {
  constructor(public picture: string) {
  }
}

