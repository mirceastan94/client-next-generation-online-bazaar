export class User {
  constructor(public id: string, public name: string, public email: string, public role: string) {
  }
}

export class UserRoleDto {
  constructor(public userId: string, public role: string) {
  }
}
