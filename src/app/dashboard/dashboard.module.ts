import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { UsersComponent } from './users/users.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { SharedModule } from '../shared/shared.module';
import { StatisticsComponent } from './statistics/statistics.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CreateOrEditDialogComponent } from './campaigns/create-or-edit-dialog/create-or-edit-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';


@NgModule({
  declarations: [DashboardComponent, UsersComponent, CampaignsComponent, StatisticsComponent, CreateOrEditDialogComponent],
  imports: [
    SharedModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    NgxChartsModule,
    AgGridModule.withComponents([])
  ],
})
export class DashboardModule { }
