import { forkJoin, ReplaySubject } from "rxjs";
import { NgxTransService } from "src/app/shared/translate/translate.service";
import { ItemResource } from "../model/products.model";

export class HeaderConfig {

  public headerResource = new ReplaySubject<ItemResource>();
  public headerTranslations: any[] = [];

  private readonly headerMap = new Map()
    .set("admin-dashboard", { uri: "/dashboard", icon: "tachometer-alt" })
    .set("account-settings", { uri: "/profile", icon: "user" })
    .set("about-us", { uri: "/about", icon: "address-book" })
    .set("logout", { uri: "N/A", icon: "power-off" })
    .set("view-label", { uri: "/products/cart-details", icon: "N/A" })
    .set("remove-item-label", { uri: "N/A", icon: "minus-square" })
    .set("checkout-label", { uri: "/products/cart-details", icon: "N/A" });

  private readonly MENU_LABEL = "menu";
  private readonly PRODUCTS_LABEL = "products";
  private readonly INDEX_LABEL = "index";
  private readonly CART_LABEL = this.PRODUCTS_LABEL.concat(".").concat(this.INDEX_LABEL).concat(".cart");

  constructor(private translateService: NgxTransService) {
    this.getHeaderModel();
  }

  public getHeaderModel() {
    forkJoin(
      [this.translateService.getKeyTranslation(this.MENU_LABEL),
      this.translateService.getKeyTranslation(this.CART_LABEL),
      ]).subscribe(translations => {
        this.headerTranslations = translations;
        this.headerTranslations.forEach(currentHeader => {
          Object.keys(currentHeader).forEach((currentTranslation: any) => {
            this.headerResource.next({
              key: currentTranslation,
              name: currentHeader[currentTranslation],
              uri: this.headerMap.get(currentTranslation) ? this.headerMap.get(currentTranslation).uri : "N/A",
              icon: this.headerMap.get(currentTranslation) ? this.headerMap.get(currentTranslation).icon : "N/A"
            });
          });
        });
      });
  }

}
