import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LogService } from 'src/app/shared/logging/log.service';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { HeaderService } from '../header.service';
import { Message } from './model/message';

@Component({
  selector: 'app-about',
  templateUrl: './about-contact.component.html',
  styleUrls: ['./about-contact.component.scss']
})
export class AboutContactComponent implements OnInit {

  messageModel: Message;
  messageForm: FormGroup;
  submitted = false;
  displayMessage = { content: "", type: "success" };

  constructor(private formBuilder: FormBuilder, private logger: LogService, private headerService: HeaderService, private translateService: NgxTransService) {
    this.messageForm = this.formBuilder.group({
      name: ['', Validators.required],
      subject: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      content: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.messageModel = new Message();
  }

  get form() {
    return this.messageForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.messageForm.valid) {
      this.headerService.sendMessage(Object.assign(this.messageModel, this.messageForm.value)).subscribe(
        (responseData) => {
          this.displayMessage.content = this.translateService.getMessageTranslation(responseData.message);
          this.displayMessage.type = "success";
        },
        (error) => {
          this.logger.log("Error received while sending the message: " + error);
          this.displayMessage.content = this.translateService.getMessageTranslation(error);
          this.displayMessage.type = "error";
        });
    }
  }

}
