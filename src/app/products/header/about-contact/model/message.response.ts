export interface MessageResponseData {
  success: boolean;
  message: string;
}
