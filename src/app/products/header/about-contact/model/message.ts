export class Message {
  name: string;
  subject: string;
  email: string;
  phone: string;
  content: string;
}
