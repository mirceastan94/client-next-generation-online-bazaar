import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LogService } from '../../shared/logging/log.service';
import { Message } from './about-contact/model/message';
import { MessageResponseData } from './about-contact/model/message.response';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(private httpClient: HttpClient, private logger: LogService) { }

  sendMessage(message: Message) {
    return this.httpClient.post<MessageResponseData>(environment.productsApiUrl.concat("/contact"), message)
      .pipe(catchError(() => { return throwError('unknown-error') }),
        tap(messageResponse => {
          this.logger.log("Response message: " + messageResponse.message);
          messageResponse.message = 'success';
        }));
  }

}
