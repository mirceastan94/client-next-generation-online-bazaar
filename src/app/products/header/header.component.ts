import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, mergeMap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { ItemResource, Product } from '../model/products.model';
import { ProductsService } from '../products.service';
import { HeaderConfig } from './header.config';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  headerCfg: HeaderConfig;
  searchedProducts: Product[];
  menuItems: ItemResource[] = [];
  otherItems: ItemResource[] = [];

  logoutResource: ItemResource;
  searchResource: ItemResource;

  public eventSubject = new Subject<string>();

  public readonly USER_DATA_LABEL: string = 'userData';
  public readonly ADMIN_ROLE: string = 'ROLE_ADMIN';

  private readonly userMenuStrings = Array.of("account-settings", "about-us");
  private readonly adminMenuStrings = Array.of("admin-dashboard", ...this.userMenuStrings);

  constructor(private productsService: ProductsService, private authService: AuthService, private translateService: NgxTransService) {
    this.headerCfg = new HeaderConfig(translateService);
  }

  ngOnInit(): void {
    const loggedUser = localStorage.getItem(this.USER_DATA_LABEL) ? JSON.parse(localStorage.getItem(this.USER_DATA_LABEL)) : JSON.parse(sessionStorage.getItem(this.USER_DATA_LABEL));
    this.headerCfg.headerResource.subscribe((currentItem: ItemResource) => {
      if (loggedUser['userDto']['role'] && loggedUser['userDto']['role'] === this.ADMIN_ROLE) {
        this.adminMenuStrings.includes(currentItem.key) ? this.menuItems.push(currentItem) : this.otherItems.push(currentItem);
      } else {
        this.userMenuStrings.includes(currentItem.key) ? this.menuItems.push(currentItem) : this.otherItems.push(currentItem);
      }
        this.logoutResource = this.otherItems?.find(currentItem => currentItem.key === "logout");
        this.searchResource = this.otherItems?.find(currentItem => currentItem.key === "search-label");
      });

    this.eventSubject.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      filter(searchedText => searchedText.length > 2),
      mergeMap(searchedText =>
        this.productsService.retrieveProducts({
          filterCriteria: { "name": searchedText },
          page: 0,
          size: 3,
          sortByField: "id",
          order: "ASC"
        })
      )).subscribe(products => this.searchedProducts = products);
  }

  ngOnDestroy(): void {
    this.eventSubject.unsubscribe();
  }

  searchProducts(event: any) {
    if (!event.target.value) {
      this.searchedProducts = null;
    }
    this.eventSubject.next(event.target.value);
  }

  logOut() {
    this.authService.logout();
  }

}
