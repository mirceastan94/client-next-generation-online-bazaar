import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/auth/model/user.model';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { Criterion, Product } from '../model/products.model';
import { Review, ReviewInfo, ReviewSummary } from '../model/reviews.model';
import { ProductsService } from '../products.service';
import { formatDate } from '@angular/common';
import { MessageResponseData } from '../header/about-contact/model/message.response';
import { CartService } from '../cart/cart.service';
import { CartProduct } from '../model/cart.model';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public loggedUser: User;
  public product: Product;
  public reviewsSummary: any;
  public reviewList: Review[];
  public reviewInfos: ReviewInfo[] = [];
  public relatedProducts: Product[] = [];
  public informationMap: Map<string, string> = new Map();
  public productInfoTranslations: any[];
  public searchCriterion: Criterion = {
    filterCriteria: {},
    page: 0,
    size: 5,
    sortByField: "name",
    order: "ASC"
  };
  public productStarsCount: number[] = [];
  public reviewAlreadyPublished: boolean = false;
  public publishingReview: boolean = false;
  public reviewContent: string;
  public productRating: any = 5;
  public todayDate: string;
  public verifiedPurchase: boolean = false;

  public statusMessage: string = "";
  public responseMessage: MessageResponseData;

  public readonly RELATED_PRODUCTS_LABEL: string = "relatedProducts";
  public readonly INFORMATION_LABEL: string = "information";
  public readonly REVIEWS_LABEL: string = "reviews";
  public readonly PRODUCT_DETAILS_LABEL: string = "products.details";
  public readonly INFORMATION_TAB: string = "information-tab";
  public readonly REVIEWS_TAB: string = "reviews-tab";
  public readonly RELATED_PRODUCTS_TAB: string = "related-products-tab";
  public readonly LABEL: string = "label";
  public readonly DOT: string = ".";
  public readonly DASH: string = "-";

  public readonly DATE_FORMAT = "yyyy-MM-dd";

  public readonly EXCELLENT_REVIEWS_LABEL: string = "excellent";
  public readonly GOOD_REVIEWS_LABEL: string = "good";
  public readonly AVERAGE_REVIEWS_LABEL: string = "average";
  public readonly BAD_REVIEWS_LABEL: string = "bad";
  public readonly HORRIBLE_REVIEWS_LABEL: string = "horrible";

  public selectedNavTab: string = "information";
  public reviewsCategoriesTrans: any;
  public infoTabTranslation: string;
  public reviewsTabTranslation: string;
  public relatedProductsTabTranslation: string;
  public navTabsMap: Map<string, string> = new Map();
  public reviewsRatingMap: Map<string, number> = new Map()
    .set(this.EXCELLENT_REVIEWS_LABEL, 5)
    .set(this.GOOD_REVIEWS_LABEL, 4)
    .set(this.AVERAGE_REVIEWS_LABEL, 3)
    .set(this.BAD_REVIEWS_LABEL, 2)
    .set(this.HORRIBLE_REVIEWS_LABEL, 1);

  constructor(private productService: ProductsService, private route: ActivatedRoute, private router: Router,
    private cartService: CartService, private translateService: NgxTransService) {
  }

  ngOnInit(): void {
    this.translateService.getKeyTranslation(this.PRODUCT_DETAILS_LABEL.concat(this.DOT).concat(this.INFORMATION_TAB))
      .subscribe(translatedStrings => {
        this.productInfoTranslations = translatedStrings;
      });
    this.route.params.subscribe(routeParams => {
      this.productService.findProduct(routeParams.id).subscribe((foundProduct: Product) => {
        this.product = foundProduct;
        this.productStarsCount = [...Array(Math.round(this.product.rating)).keys()].map(count => ++count);
        Object.assign(this.searchCriterion.filterCriteria, this.product.subCategory
          ? { 'subcategory': this.product.subCategory } : { 'category': this.product.category });
        this.initializeInfoWithTranslation(this.product);
        if (routeParams.campaignId) {
          this.productService.findProductWithCampaign(routeParams.id, routeParams.campaignId).subscribe((campaignProduct) => {
            this.product.discountPrice = campaignProduct.discountPrice;
            this.product.discountPercentage = this.calculatePriceReductionPercentage(this.product);
          });
        }
        else if (this.product.discountPrice) {
          this.product.discountPercentage = this.calculatePriceReductionPercentage(this.product);
        }
      });
    });

    this.translateService.getKeyTranslation(this.PRODUCT_DETAILS_LABEL.concat(this.DOT).concat(this.INFORMATION_TAB).concat(this.DOT)
      .concat(this.LABEL)).subscribe(translation => {
        this.infoTabTranslation = translation;
      })
    this.translateService.getKeyTranslation(this.PRODUCT_DETAILS_LABEL.concat(this.DOT).concat(this.REVIEWS_TAB).concat(this.DOT)
      .concat(this.LABEL)).subscribe(translation => {
        this.reviewsTabTranslation = translation;
      })
    this.translateService.getKeyTranslation(this.PRODUCT_DETAILS_LABEL.concat(this.DOT).concat(this.RELATED_PRODUCTS_TAB).concat(this.DASH)
      .concat(this.LABEL)).subscribe(translation => {
        this.relatedProductsTabTranslation = translation;
      })
    this.navTabsMap.set(this.INFORMATION_LABEL, this.infoTabTranslation)
      .set(this.REVIEWS_LABEL, this.reviewsTabTranslation)
      .set(this.RELATED_PRODUCTS_LABEL, this.relatedProductsTabTranslation);

    this.todayDate = formatDate(new Date(), this.DATE_FORMAT, "en");
  }

  private initializeInfoWithTranslation(product: any): void {
    Object.entries(this.productInfoTranslations).forEach(([key, value]) => {
      if (key !== this.LABEL) {
        this.informationMap.set(value, product[key]);
      }
    });
    this.productService.getProductReviews(this.product.id).subscribe((reviewsWithStats: ReviewSummary) => {
      this.reviewInfos = [];
      this.reviewAlreadyPublished = false;
      this.reviewsSummary = reviewsWithStats;
      this.reviewList = reviewsWithStats.reviewDtos;
      this.checkForPublishedReview(this.reviewList.map(review => review.userDto.email));
      this.translateService.getKeyTranslation(this.PRODUCT_DETAILS_LABEL.concat(this.DOT).concat(this.REVIEWS_TAB)).subscribe(translation => {
        this.reviewsCategoriesTrans = translation;
        this.reviewsRatingMap.forEach((starsCount, category) => {
          const categoryTrans = this.reviewsCategoriesTrans[category];
          const starsArray = [...Array(Math.round(starsCount)).keys()].map(count => ++count);
          this.reviewInfos.push(new ReviewInfo(categoryTrans, starsArray, this.reviewsSummary[category]));
        });
      });
    });
    this.productService.retrieveProducts(this.searchCriterion).subscribe((products: Product[]) => {
      this.relatedProducts = [];
      products.forEach((currentProduct: Product) => {
        this.productService.getProductReviewsCount(currentProduct.id).subscribe((reviewsCount: number) => {
          currentProduct.reviewsCount = reviewsCount;
        });
        if (this.product.id !== currentProduct.id) {
          currentProduct.discountPercentage = this.calculatePriceReductionPercentage(currentProduct);
          this.relatedProducts.push(currentProduct);
        }
      });
    });
  }

  private calculatePriceReductionPercentage(product: Product): number {
    const priceReduction = product.price - product.discountPrice;
    return priceReduction / product.price * 100;
  }

  public buyProduct(): void {
    this.cartService.addOrUpdateCart(this.product, 1, false);
    this.router.navigateByUrl('/products/cart-details');
  }

  public addToCart(): void {
    this.cartService.addOrUpdateCart(this.product, 1, false);
  }

  public addRelatedProductToCart(product: Product): void {
    this.cartService.addOrUpdateCart(product, 1, false);
  }

  public toggleReviewPublishing(): void {
    this.publishingReview = !this.publishingReview;
  }

  /**
   * This method will check whether the logged in user already published a review for this product
   */
  public checkForPublishedReview(authors: string[]): void {
    this.loggedUser = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).userDto : JSON.parse(sessionStorage.getItem('userData')).userDto;
    if (authors.includes(this.loggedUser.email)) {
      this.reviewAlreadyPublished = true;
    }
  }

  public publishReview(): void {
    if (this.reviewContent.length < 50) {
      this.statusMessage = this.translateService.getMessageTranslation("review-too-short");
    } else if (this.reviewAlreadyPublished) {
      this.responseMessage.success = false;
      this.statusMessage = this.translateService.getMessageTranslation("review-already-published");
    } else {
      const review: Review = new Review(this.reviewContent, this.todayDate, this.productRating, this.verifiedPurchase, this.loggedUser, this.product.id);
      this.productService.publishReview(review).subscribe((responseData: any) => {
        this.responseMessage = responseData;
        if (this.responseMessage.success) {
          this.statusMessage = this.translateService.getMessageTranslation("review-published");
          this.reviewAlreadyPublished = true;
        } else {
          this.statusMessage = this.translateService.getMessageTranslation("unknown-error");
        }
      });
    }
  }
}
