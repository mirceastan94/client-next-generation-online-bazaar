import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProductsComponent } from './products.component';
import { RouterModule } from '@angular/router';
import { ProductsRoutingModule } from './products-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IndexComponent } from './index/index.component';
import { ProfileComponent } from './profile/profile.component';
import { GridComponent } from './grid/grid.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { DetailsComponent } from './details/details.component';
import { BarRatingModule } from "ngx-bar-rating";
import { CartComponent } from './cart/cart.component';
import { CartDetailsComponent } from './cart/cart-details/cart-details.component';
import { CartOrderDialog } from './cart/cart-details/order-dialog/cart-order.components';
import { AboutContactComponent } from './header/about-contact/about-contact.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [ProductsComponent, HeaderComponent, IndexComponent, AboutContactComponent, ProfileComponent,
    GridComponent, DetailsComponent, CartComponent, CartDetailsComponent, CartOrderDialog],
  imports: [RouterModule, ProductsRoutingModule, SharedModule, FormsModule, ReactiveFormsModule, NgxSliderModule, BarRatingModule],
  exports: [HeaderComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProductsModule { }
