import { Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, NavigationError, NavigationStart, ParamMap, Router, RouterEvent } from '@angular/router';
import { withLatestFrom } from 'rxjs/operators';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { Category, Criterion, Product, Translation } from '../model/products.model';
import { ProductsService } from '../products.service';
import { GridConfig } from './grid.config';
import { CartService } from '../cart/cart.service';
import { CampaignDto } from '../model/campaign.model';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  public readonly showCounts = [5, 10, 15, 20];
  public slidersInitialized: boolean = false;
  public foundProducts: Product[] = [];
  public minPriceProduct: number = 0;
  public maxPriceProduct: number = 0;
  public sliderMinPrice: number = 0;
  public sliderMaxPrice: number = 0;
  public currentUrl: string;
  public campaignId: string;
  public campaign: CampaignDto;

  public currentItemsCount: number;

  private readonly YEAR_DATE_LABEL = '2021-01-01';
  public readonly currentYearDate: string = new Date(this.YEAR_DATE_LABEL).toISOString().split('T')[0];
  public readonly sortByNames: string[] = [];
  public readonly sortOrders: Translation[] = [];
  public readonly categories: Category[] = [];
  public readonly gridConfig: GridConfig;
  public readonly brands: string[] = [];
  public readonly NOT_AVAILBLE = "N/A";
  public readonly SPECIAL_CATEGORIES = "special";
  public readonly CAMPAIGN = "campaign";
  public readonly COMMA = ",";

  public readonly defaultCriterion: Criterion =
    {
      filterCriteria: {},
      page: 0,
      size: 25,
      sortByField: "brand",
      order: "ASC"
    };
  public currentSearchCriterion: Criterion =
    {
      filterCriteria: {},
      page: 0,
      size: 20,
      sortByField: "name",
      order: "ASC"
    };

  constructor(private productsService: ProductsService, private router: Router, private route: ActivatedRoute,
    private cartService: CartService, private translateService: NgxTransService) {
    this.gridConfig = new GridConfig(translateService);
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        delete this.currentSearchCriterion.filterCriteria['price'];
      }
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url.toString();
        this.slidersInitialized = false;
      }
    });
  }

  ngOnInit(): void {
    this.gridConfig.sortTranslation$.subscribe((sortFilterTranslation: string) => {
      this.sortByNames.push(sortFilterTranslation);
    });
    this.gridConfig.orderTranslation$.subscribe((orderTranslation: Translation) => {
      this.sortOrders.push(orderTranslation);
    });
    this.gridConfig.categoryTranslation$.subscribe((currentCategory: Category) => {
      this.categories.push(currentCategory);
    });
    this.productsService.retrieveProducts(this.defaultCriterion).subscribe((products) => {
      products.forEach(currentProduct => {
        if (currentProduct.brand != this.NOT_AVAILBLE && !this.brands.includes(currentProduct.brand)) {
          this.brands.push(currentProduct.brand);
        }
      });
    });
    this.route.url.pipe(withLatestFrom(this.route.paramMap, this.route.queryParamMap)).subscribe(([url, paramMap, queryParamMap]) => {
      if (url.toString().includes(this.SPECIAL_CATEGORIES)) {
        this.currentSearchCriterion = this.gridConfig.specialCriteria.get(url.toString().split(this.COMMA)[1]);
        this.retrieveProductsWithCurrentCriterion();
      } else if (url.toString().includes(this.CAMPAIGN)) {
        this.campaignId = url.toString().split(this.COMMA)[1];
        this.productsService.retrieveCampaign(this.campaignId).subscribe((foundCampaign) => {
          this.campaign = foundCampaign;
          const productsIds = this.campaign.campaignProducts.map(product => product.name);
          this.currentSearchCriterion = this.generateCriterionByNames(productsIds);
          this.retrieveProductsWithCurrentCriterion();
        })
      }
      else {
        Object.values(paramMap).forEach((parameter) => {
          Object.assign(this.currentSearchCriterion.filterCriteria, parameter);
        });
        Object.values(queryParamMap).forEach((parameter) => {
          Object.assign(this.currentSearchCriterion.filterCriteria, parameter);
        });
        this.retrieveProductsWithCurrentCriterion();
      }
    });
  }

  onChangeShowCount(showCount: any): void {
    this.currentSearchCriterion.size = parseInt(showCount.target.value);
    this.retrieveProductsWithCurrentCriterion();
  }

  onChangeSortName(sortName: any): void {
    this.currentSearchCriterion.sortByField = sortName.target.value.toLowerCase() === 'date added' ? 'introductoryDate' : sortName.target.value.toLowerCase();
    this.retrieveProductsWithCurrentCriterion();
  }

  onChangeSortOrder(sortOrder: any): void {
    const orderKey = this.sortOrders.find(order => order.label == sortOrder.target.value).key.toUpperCase();
    this.currentSearchCriterion.order = orderKey;
    this.retrieveProductsWithCurrentCriterion();
  }

  onChangeMinPrice(minPrice: any): void {
    this.sliderMinPrice = minPrice.target.value.length > 1 ? minPrice.target.value : this.minPriceProduct;
    const priceCriteria = {
      "price": this.sliderMinPrice.toString().concat(this.COMMA).concat(this.sliderMaxPrice.toString())
    };
    Object.assign(this.currentSearchCriterion.filterCriteria, priceCriteria);
    this.retrieveProductsWithCurrentCriterion();
  }

  onChangeMaxPrice(maxPrice: any): void {
    this.sliderMaxPrice = maxPrice.target.value.length > 1 ? maxPrice.target.value : this.maxPriceProduct;
    const priceCriteria = {
      "price": this.sliderMinPrice.toString().concat(this.COMMA).concat(this.sliderMaxPrice.toString())
    };
    Object.assign(this.currentSearchCriterion.filterCriteria, priceCriteria);
    this.retrieveProductsWithCurrentCriterion();
  }

  handlePageChange(currentPage: any): void {
    this.currentSearchCriterion.page = currentPage - 1;
    this.retrieveProductsWithCurrentCriterion();
  }

  private retrieveProductsWithCurrentCriterion(): void {
    this.productsService.retrieveProducts(this.currentSearchCriterion).subscribe((products) => {
      this.foundProducts = [];
      this.foundProducts.push(...products);
      if (Array.isArray(this.foundProducts) && this.foundProducts.length) {
        this.calculatePriceRanges();
        this.calculatePriceReductionPercentage();
      }
    });
    this.productsService.countProducts(this.currentSearchCriterion).subscribe((count) => {
      this.currentItemsCount = count;
    });
  }

  private calculatePriceRanges(): void {
    const productsCopy: Product[] = JSON.parse(JSON.stringify(this.foundProducts));
    productsCopy.sort((firstProduct, secondProduct) => firstProduct.price - secondProduct.price);
    this.minPriceProduct = productsCopy[0].price;
    this.maxPriceProduct = productsCopy[productsCopy.length - 1].price;
    if (!this.slidersInitialized) {
      this.sliderMinPrice = this.minPriceProduct;
      this.sliderMaxPrice = this.maxPriceProduct;
    }
    this.slidersInitialized = true;
  }

  private calculatePriceReductionPercentage(): void {
    this.foundProducts.forEach((currentProduct) => {
      const priceReduction = currentProduct.price - currentProduct.discountPrice;
      currentProduct.discountPercentage = priceReduction / currentProduct.price * 100;
    });
  }

  public addToCart(product: Product): void {
    this.cartService.addOrUpdateCart(product, 1, false);
  }

  private generateCriterionByNames(productNames: string[]): Criterion {
    return {
      filterCriteria: { "name": productNames.join() },
      page: 0,
      size: 20,
      sortByField: "name",
      order: "ASC",
      campaignId: this.campaignId
    };
  }

}
