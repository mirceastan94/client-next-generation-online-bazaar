import { ReplaySubject } from "rxjs";
import { NgxTransService } from "src/app/shared/translate/translate.service";
import { Category, Criterion, Translation } from "../model/products.model";

export class GridConfig {

  public sortTranslation$ = new ReplaySubject<string>();
  public orderTranslation$ = new ReplaySubject<Translation>();
  public categoryTranslation$ = new ReplaySubject<Category>();
  public filtersTranslations: any[] = [];
  public orderTranslations: any[] = [];
  public categoriesTranslations: any[] = [];
  private readonly SORT_NAMES_LABEL = "products.grid.top-panel.sort";
  private readonly ORDER_NAMES_LABEL = "products.grid.top-panel.order";
  private readonly CATEGORIES_LABEL = "products.grid.categories";

  private readonly HOT_DEALS_LABEL = "hot-deals";
  private readonly RECENT_ARRIVALS_LABEL = "recent-arrivals";

  public specialCriteria = new Map<String, Criterion>()
    .set(this.HOT_DEALS_LABEL, {
      filterCriteria: {},
      page: 0,
      size: 20,
      sortByField: "id",
      order: "ASC"
    })
    .set(this.RECENT_ARRIVALS_LABEL,
      {
        filterCriteria: { "introductoryDate": "2021-01-01" },
        page: 0,
        size: 20,
        sortByField: "id",
        order: "ASC"
      }
    );

  constructor(private translateService: NgxTransService) {
    this.initializeTranslations();
  }

  public initializeTranslations(): void {
    this.translateService.getKeyTranslation(this.SORT_NAMES_LABEL).subscribe((translatedStrings) => {
      this.filtersTranslations = translatedStrings;
      Object.values(this.filtersTranslations).forEach((filterTranslation) => {
        this.sortTranslation$.next(filterTranslation);
      });
    });
    this.translateService.getKeyTranslation(this.ORDER_NAMES_LABEL).subscribe((translatedStrings) => {
      this.orderTranslations = translatedStrings;
      Object.entries(this.orderTranslations).forEach(([orderTranslationKey, orderTranslationValue]) => {
        this.orderTranslation$.next(new Translation(orderTranslationKey, orderTranslationValue));
      });
    });
    this.translateService.getKeyTranslation(this.CATEGORIES_LABEL).subscribe((translatedStrings) => {
      this.categoriesTranslations = translatedStrings;
      Object.entries(this.categoriesTranslations).forEach(([key, value]) => {
        this.categoryTranslation$.next({
          key: key,
          name: value,
          subSections: null
        });
      });
    })
  };
}
