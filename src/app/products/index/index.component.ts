import { Component, OnInit } from '@angular/core';
import { LogService } from 'src/app/shared/logging/log.service';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { ProductsService } from '../products.service';
import { Category, Collection, Criterion, DescriptionResource, Headline, Product, Section } from '../model/products.model';
import { ProductsConfig } from '../products.config';
import { CartService } from '../cart/cart.service';
import { CampaignStatus } from '../model/campaign.model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  public productsCfg: ProductsConfig;
  public searchCriteria: Criterion[] = [];
  public fashionProducts: Product[] = [];
  public booksProducts: Product[] = [];
  public footerResource: DescriptionResource[] = [];
  public headlineResource: Headline[] = [];
  public productCategoriesResource: Category[] = [];
  public specialCategoriesResource: Category[] = [];
  public campaignCategoriesResource: Category[] = [];
  public collectionsResource: Collection[] = [];
  public selectedProductType: string;

  private readonly INDEX_TRENDING = "IndexTrending";
  private readonly NAME = "name";
  public readonly FASHION_LABEL = "fashion";
  public readonly BOOKS_LABEL = "books";

  public productTypes: string[] = [this.FASHION_LABEL, this.BOOKS_LABEL];

  constructor(private productsService: ProductsService, private cartService: CartService, private translateService: NgxTransService) {
    this.productsCfg = new ProductsConfig(productsService, translateService);
    this.searchCriteria = this.productsCfg.criteriaModel;
  }

  ngOnInit(): void {
    this.selectedProductType = this.FASHION_LABEL;

    this.productsCfg.headline.subscribe((headline: Headline) => {
      this.headlineResource.push(headline);
    });

    this.productsCfg.categories.subscribe((category: Category) => {
      this.productsCfg.SPECIAL_CATEGORIES_NAMES.includes(category.key) ? this.specialCategoriesResource.push(category) : this.productCategoriesResource.push(category);
    });

    this.productsCfg.collections.subscribe((collection: Collection) => {
      this.collectionsResource.push(collection);
    });

    const trendingCriterion = this.searchCriteria.find(criterion => criterion.name === this.INDEX_TRENDING);
    delete trendingCriterion[this.NAME];
    this.productsService.retrieveProducts(trendingCriterion).subscribe((products) => {
      products.forEach(currentProduct => currentProduct.category.toLowerCase() === this.FASHION_LABEL ? this.fashionProducts.push(currentProduct) : this.booksProducts.push(currentProduct));
    });

    this.productsService.retrieveCampaigns().subscribe((campaignList) => {
      campaignList.forEach((campaign) => {
        if (campaign.status.toString() === 'active') {
          this.campaignCategoriesResource.push(new Category(campaign.id, campaign.name, []));
        }
      });
    })
  }

  public addToCart(product: Product): void {
    this.cartService.addOrUpdateCart(product, 1, false);
  }

}
