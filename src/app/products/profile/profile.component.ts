import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/auth/model/user.model';
import { LogService } from 'src/app/shared/logging/log.service';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { PasswordValidator } from 'src/app/shared/utils/password-validator';
import { ProductsService } from '../products.service';
import { Profile } from './model/profile';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  loggedUser: { accessToken: string, expiryDate: Date, tokenType: string, userDto: User };
  profile: Profile;
  profileForm: FormGroup;
  submitted = false;
  statusMessage = { content: "", type: "success" };
  alignClass = "language-toolbar";
  expiryDate: string;
  language: string;
  cardImageResourcePath: string;


  constructor(private formBuilder: FormBuilder, private productsService: ProductsService, private authService: AuthService,
    private translateService: NgxTransService, private logger: LogService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.logger.log('Retrieving profile information...');
    this.loggedUser = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : JSON.parse(sessionStorage.getItem('userData'));
    this.productsService.getProfile(this.loggedUser.userDto.email).subscribe(
      (profileInfo) => {
        this.profile = profileInfo;
        this.expiryDate = this.profile.userDetailsDto?.paymentCardDto?.expiryDate;
        this.profileForm = this.formBuilder.group({
          name: [this.profile.name, Validators.required],
          email: [this.profile.email, [Validators.required, Validators.email]],
          phone: [this.profile.userDetailsDto?.phone],
          address: [this.profile.userDetailsDto?.address],
          place: [this.profile.userDetailsDto?.place],
          picture: [this.profile.userDetailsDto?.picture],
          password: [this.profile.password],
          confirmPassword: [this.profile.password],
          cardOwnerName: [this.profile.userDetailsDto?.paymentCardDto?.ownerName],
          cardNumber: [this.profile.userDetailsDto?.paymentCardDto?.number],
          cardExpiryDate: [this.expiryDate],
          cardCvvCvcCode: [this.profile.userDetailsDto?.paymentCardDto?.cvvCvcCode],
        });
        this.profileForm.setValidators(PasswordValidator.passwordsMatch("password", "confirmPassword"));
        this.logger.log('Profile information retrieved...');
        this.statusMessage.type = "success";
        this.cdRef.detectChanges();
      },
      (error) => {
        this.statusMessage.content = error;
        this.statusMessage.type = "error";
      });
  }

  get form() {
    return this.profileForm.controls;
  }

  public onSubmit() {
    this.submitted = true;
    if (this.profileForm.valid) {
      this.logger.log('Sending updated profile data...');
      this.language = this.translateService.getCurrentLanguage();
      this.assignFormDataToProfile(this.profile);
      this.productsService.updateProfile(this.profile).subscribe(
        (responseData) => {
          this.logger.log('Profile data has been updated!');
          this.statusMessage.content = this.translateService.getMessageTranslation(responseData.message);
          this.statusMessage.type = "success";
          if (responseData.loginRequired) {
            setTimeout(() => {
              this.authService.logout();
            }, 5000)
          }
        },
        (error) => {
          this.statusMessage.content = this.translateService.getMessageTranslation(error);
          this.statusMessage.type = "error";
        });
    }
  }

  public assignFormDataToProfile(profile: Profile) {
    const profileForm = this.profileForm.value;

    if (profileForm.email != profile.email) {
      profile.previousEmail = profile.email;
      profile.emailChanged = true;
    } else {
      profile.emailChanged = false;
    }
    profile.email = profileForm.email;
    profile.name = profileForm.name;
    profile.password = profileForm.password;
    profile.userDetailsDto.address = profileForm.address;
    profile.userDetailsDto.phone = profileForm.phone;
    profile.userDetailsDto.place = profileForm.place;
    profile.userDetailsDto.language = this.language;
    profile.userDetailsDto.picture = profileForm.picture;
    const paymentCardDto = {
      ownerName: profileForm.cardOwnerName,
      number: profileForm.cardNumber,
      expiryDate: profileForm.cardExpiryDate,
      cvvCvcCode: profileForm.cardCvvCvcCode
    }
    profile.userDetailsDto.paymentCardDto = paymentCardDto;

    this.loggedUser.userDto.userDetailsDto.picture = profileForm.picture;
    localStorage.getItem('userData')
      ? localStorage.setItem('userData', JSON.stringify(this.loggedUser))
      : sessionStorage.setItem('userData', JSON.stringify(this.loggedUser));
  }

  public uploadFile(event: any) {
    let reader = new FileReader();
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.profileForm.patchValue({
          picture: reader.result
        });
      }
    }
  }

  public onCardNumberInputChange(event: any): void {
    if (event.target.value.startsWith('4')) {
      this.cardImageResourcePath = "../../../assets/images/card/visa-logo.png";
    } else if (event.target.value.startsWith('5')) {
      this.cardImageResourcePath = "../../../assets/images/card/mastercard-logo.png";
    }
  }

}

