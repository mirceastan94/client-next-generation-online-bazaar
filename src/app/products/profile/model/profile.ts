export class Profile {
  name: string;
  email: string;
  previousEmail: string;
  emailChanged: boolean;
  password: string;
  role: string;
  userDetailsDto: {
    phone: string;
    address: string;
    place: string;
    picture: string;
    language: string;
    paymentCardDto: {
      ownerName: string;
      number: string;
      expiryDate: string;
      cvvCvcCode: number;
    }
  };
}
