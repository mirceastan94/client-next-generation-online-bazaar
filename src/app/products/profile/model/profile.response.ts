export interface ProfileResponseData {
  success: boolean;
  message: string;
  loginRequired: string;
}
