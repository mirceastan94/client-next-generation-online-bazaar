import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LogService } from '../shared/logging/log.service';
import { MessageResponseData } from './header/about-contact/model/message.response';
import { CampaignDto } from './model/campaign.model';
import { Criterion, Product } from './model/products.model';
import { Review, ReviewSummary } from './model/reviews.model';
import { Profile } from './profile/model/profile';
import { ProfileResponseData } from './profile/model/profile.response';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpClient: HttpClient, private logger: LogService) { }

  retrieveProducts(criterion: Criterion) {
    return this.httpClient.post<Product[]>(environment.productsApiUrl.concat("/find-by-criteria"), criterion).pipe(catchError((this.handleError)));
  }

  findProduct(id: string) {
    return this.httpClient.get<Product>(environment.productsApiUrl.concat("/find/").concat(id)).pipe(catchError((this.handleError)));
  }

  findProductWithCampaign(id: string, campaignId: string) {
    return this.httpClient.get<Product>(environment.productsApiUrl.concat("/find/").concat(id).concat("/campaign/")
      .concat(campaignId)).pipe(catchError((this.handleError)));
  }

  getProductReviews(id: string) {
    return this.httpClient.get<ReviewSummary>(environment.productsApiUrl.concat("/review/retrieve/").concat(id)).pipe(catchError((this.handleError)));
  }

  getProductReviewsCount(id: string) {
    return this.httpClient.get<number>(environment.productsApiUrl.concat("/review/count/").concat(id)).pipe(catchError((this.handleError)));
  }

  publishReview(message: Review) {
    return this.httpClient.post<MessageResponseData>(environment.productsApiUrl.concat("/review/publish"), message)
      .pipe(catchError(() => { return throwError('unknown-error') }),
        tap(messageResponse => {
          this.logger.log("Response message: " + messageResponse.message);
          messageResponse.message = 'success';
        }));
  }

  countProducts(criterion: Criterion) {
    return this.httpClient.post<number>(environment.productsApiUrl.concat("/count-by-criteria"), criterion).pipe(catchError((this.handleError)));
  }

  getProfile(email: string) {
    const emailParam = new HttpParams().set('email', email);
    return this.httpClient.get<Profile>(environment.productsApiUrl.concat("/user/profile"), { params: emailParam }).pipe(catchError((this.handleError)))
  }

  updateProfile(profile: Profile) {
    return this.httpClient.post<ProfileResponseData>(environment.productsApiUrl.concat("/user/profile"), profile).pipe(catchError((this.handleError)),
      tap(messageResponse => {
        this.logger.log("Response message: " + messageResponse.message);
        messageResponse.message = messageResponse.loginRequired ? 'success-relogin' : 'success';
      }));
  }

  retrieveCampaigns(): Observable<CampaignDto[]> {
    return this.httpClient.get<CampaignDto[]>(environment.productsApiUrl.concat("/campaign/find/all")).pipe(catchError(this.handleError));
  }

  retrieveCampaign(id: string): Observable<CampaignDto> {
    return this.httpClient.get<CampaignDto>(environment.productsApiUrl.concat("/campaign/find/").concat(id)).pipe(catchError(this.handleError));
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = 'unknown-error';
    if (!errorRes.error || !errorRes.error.message) {
      return throwError(errorMessage);
    }

    switch (errorRes.error.message) {
      case 'Another user with the same email exists in the database already!':
        errorMessage = 'email-already-exists';
        break;
      case 'Bad credentials':
        errorMessage = 'invalid-credentials';
        break;
    }
    console.error("Response error: " + JSON.stringify(errorRes.error));
    return throwError(errorMessage);
  }

}
