import { CartProduct } from "../../model/cart.model";

export class CartOrderResponse {

  orderId: string;
  outOfStockCartProducts: CartProduct[];
  inStockCartProducts: CartProduct[];
  totalCartPrice: number;
  success: boolean;
  errorMessage: string;

  constructor(orderId: string, outOfStockCartProducts: CartProduct[], inStockCartProducts: CartProduct[], totalCartPrice: number, success: boolean, errorMessage: string) {
    this.orderId = orderId;
    this.outOfStockCartProducts = outOfStockCartProducts;
    this.inStockCartProducts = inStockCartProducts;
    this.totalCartPrice = totalCartPrice;
    this.success = success;
    this.errorMessage = errorMessage;
   }
}
