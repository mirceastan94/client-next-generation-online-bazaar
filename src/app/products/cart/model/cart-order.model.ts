import { CartProduct } from "../../model/cart.model";
import { Profile } from "../../profile/model/profile";

export class CartOrder {

  cartProducts: CartProduct[];
  totalCartPrice: string;
  userProfile: Profile;

  constructor(cartProducts: CartProduct[], totalCartPrice: string, userProfile: Profile) {
    this.cartProducts = cartProducts;
    this.totalCartPrice = totalCartPrice;
    this.userProfile = userProfile;
   }
}
