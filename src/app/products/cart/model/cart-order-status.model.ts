export enum OrderStatus {
  InputValidation = "INPUT_VALIDATION",
  InProgress = "IN_PROGRESS",
  OutOfStockProducts = "OUT_OF_STOCK_PRODUCTS",
  PaymentUnsuccessful = "UNSUCCESSFUL_PAYMENT",
  CompletedSuccessfully = "SUCCESSFULLY_COMPLETED",
  InternalServerError = "INTERNAL_SERVER_ERROR"
}
