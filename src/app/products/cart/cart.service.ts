import { HttpClient, HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, ReplaySubject, throwError } from "rxjs";
import { catchError, delay } from "rxjs/operators";
import { LogService } from "src/app/shared/logging/log.service";
import { environment } from "src/environments/environment";
import { CartProduct } from "../model/cart.model";
import { OrderMailAction } from "../../shared/model/mail.action.model";
import { Product } from "../model/products.model";
import { CartOrderResponse } from "./model/cart-order-response.model";
import { CartOrder } from "./model/cart-order.model";
import { EmailDetails } from "src/app/shared/model/order.mail.model";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  isLocalStorage: boolean;
  public cartProducts: CartProduct[];
  public totalCartPrice: number = 0;

  public cartProducts$ = new ReplaySubject<CartProduct[]>();
  public totalCartPrice$ = new ReplaySubject<number>();

  public readonly CART_PRODUCTS_LABEL: string = 'cartProducts';

  constructor(private httpClient: HttpClient, private logger: LogService) {
    this.isLocalStorage = localStorage.getItem('userData') ? true : false;
    this.cartProducts = this.isLocalStorage ? JSON.parse(localStorage.getItem(this.CART_PRODUCTS_LABEL)) : JSON.parse(sessionStorage.getItem(this.CART_PRODUCTS_LABEL));
    if (!this.cartProducts) {
      this.cartProducts = [];
    }
    this.cartProducts$.next(this.cartProducts);
    this.totalCartPrice$.next(this.calculateCartTotalPrice());
  }

  addOrUpdateCart(chosenProduct: Product, quantity: number, updatedWithinCart: boolean): void {
    let productToUpdate = this.cartProducts.find(currentCartProduct => currentCartProduct.product.id === chosenProduct.id);
    if (!productToUpdate) {
      const newCartProduct: CartProduct = { product: chosenProduct, quantity: quantity, totalPrice: chosenProduct.discountPrice ? chosenProduct.discountPrice * quantity : chosenProduct.price * quantity };
      this.cartProducts.push(newCartProduct);
    } else {
      let productToUpdateIdx = this.cartProducts.findIndex(currentCartProduct => currentCartProduct.product.id === chosenProduct.id);
      if (updatedWithinCart) {
        productToUpdate.quantity = quantity;
        productToUpdate.totalPrice = chosenProduct.discountPrice ? chosenProduct.discountPrice * quantity : chosenProduct.price * quantity;
      } else {
        productToUpdate.quantity = productToUpdate.quantity + quantity;
        productToUpdate.totalPrice = productToUpdate.totalPrice + (chosenProduct.discountPrice ? chosenProduct.discountPrice * quantity : chosenProduct.price * quantity);
      }
      if (productToUpdate.quantity > 0) {
        this.cartProducts[productToUpdateIdx] = productToUpdate;
      }
      else {
        this.removeFromCart(this.cartProducts[productToUpdateIdx].product);
      }
    }
    this.isLocalStorage ? localStorage.setItem(this.CART_PRODUCTS_LABEL, JSON.stringify(this.cartProducts)) : sessionStorage.setItem(this.CART_PRODUCTS_LABEL, JSON.stringify(this.cartProducts));
    this.cartProducts$.next(this.cartProducts);
    this.totalCartPrice$.next(this.calculateCartTotalPrice());
  }

  removeFromCart(product: Product): void {
    this.cartProducts = this.cartProducts.filter(cartProduct => cartProduct.product.id !== product.id);
    this.isLocalStorage ? localStorage.setItem(this.CART_PRODUCTS_LABEL, JSON.stringify(this.cartProducts))
      : sessionStorage.setItem(this.CART_PRODUCTS_LABEL, JSON.stringify(this.cartProducts));
    this.cartProducts$.next(this.cartProducts);
    this.totalCartPrice$.next(this.calculateCartTotalPrice());
  }

  emptyCart(): void {
    this.cartProducts = [];
    this.isLocalStorage ? localStorage.removeItem(this.CART_PRODUCTS_LABEL) : sessionStorage.removeItem(this.CART_PRODUCTS_LABEL);
    this.cartProducts$.next(this.cartProducts);
    this.totalCartPrice$.next(0);
  }

  updateCartWithInStockProducts(updatedCartProducts: CartProduct[]): void {
    this.cartProducts = updatedCartProducts;
    this.cartProducts$.next(this.cartProducts);
    this.totalCartPrice$.next(this.calculateCartTotalPrice());
  }

  calculateCartTotalPrice(): number {
    return this.cartProducts.map(cartProduct => cartProduct?.totalPrice).reduce((previous, next) => previous + next, 0);
  }

  initiateCartOrder(cartOrderToInitiate: CartOrder): Observable<CartOrderResponse> {
    return this.httpClient.post<CartOrderResponse>(environment.ordersApiUrl.concat("/generate"), cartOrderToInitiate).pipe(delay(1000), catchError(this.handleError));
  }

  sendConfirmationEmail(orderId: string, orderMailAction: OrderMailAction): Observable<{ success: boolean, message: string }> {
    return this.httpClient.post<{ success: boolean, message: string }>(environment.ordersApiUrl.concat("/sendEmail"),
    new EmailDetails(orderId, orderMailAction))
    .pipe(catchError(this.handleError));
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = 'unknown-error';
    if (!errorRes.error || !errorRes.error.message) {
      return throwError(errorMessage);
    }

    switch (errorRes.error.message) {
      case 'Payment could not be processed due to an internal error!':
        errorMessage = 'email-already-exists';
        break;
      // case 'Some products are not in stock anymore':
      //   errorMessage = 'invalid-credentials';
      //   break;
    }
    console.error("Response error: " + JSON.stringify(errorRes.error));
    return throwError(errorMessage);
  }

}
