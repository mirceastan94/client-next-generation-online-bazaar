import { Component, EventEmitter, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { CartOrderResponse } from "../../model/cart-order-response.model";
import { OrderStatus } from "../../model/cart-order-status.model";


@Component({
  selector: 'cart-order-dialog',
  templateUrl: './cart-order.dialog.html',
  styleUrls: ['./cart-order.dialog.scss']
})
export class CartOrderDialog {

  public cartOrderResponse: CartOrderResponse;
  public readonly orderStatusValue: string;
  public readonly orderStatus = OrderStatus;

  public onConfirm = new EventEmitter();

  constructor(public dialogRef: MatDialogRef<CartOrderDialog>, @Inject(MAT_DIALOG_DATA) public dialogData: { orderStatus: string, orderResponse: CartOrderResponse },
    private router: Router) {
    this.cartOrderResponse = dialogData.orderResponse;
    this.orderStatusValue = dialogData.orderStatus;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onConfirmClick(): void {
    this.onConfirm.emit();
  }

  onViewOrderDetails(): void {
    this.dialogRef.close();
    this.router.navigate(['/orders/', this.dialogData.orderResponse.orderId]);
  }

}
