import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { User } from 'src/app/auth/model/user.model';
import { LogService } from 'src/app/shared/logging/log.service';
import { CartProduct } from '../../model/cart.model';
import { OrderMailAction } from '../../../shared/model/mail.action.model';
import { Product } from '../../model/products.model';
import { ProductsService } from '../../products.service';
import { Profile } from '../../profile/model/profile';
import { CartService } from '../cart.service';
import { CartOrderResponse } from '../model/cart-order-response.model';
import { OrderStatus } from '../model/cart-order-status.model';
import { CartOrder } from '../model/cart-order.model';
import { CartOrderDialog } from './order-dialog/cart-order.components';

@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.scss']
})
export class CartDetailsComponent implements OnInit {

  public cartOrderResponse: CartOrderResponse;
  public cartProducts: CartProduct[] = [];
  public userProfile: Profile;
  public cartTotalPrice: number = 0;
  public cartSubtotal: number = 0;
  public orderFormGroup: FormGroup;
  public loggedUser: { accessToken: string, expiryDate: Date, tokenType: string, userDto: User };
  public profileSub$: Subscription;
  public isSubmitted: boolean = false;
  public isCartEmpty: boolean = true;
  public cardImageResourcePath: string;

  public orderSuccessfullyProcessed: boolean = true;
  public currentOrderStatus: string = OrderStatus.InputValidation;

  public SHIPPING_PRICE: number = 2.99;
  public USER_DATA_LABEL: string = 'userData';
  public PAYMENT_ERROR_LABEL: string = 'error while trying to perform the payment';

  constructor(private formBuilder: FormBuilder, private cartService: CartService, private productsService: ProductsService,
    private dialog: MatDialog, private logService: LogService) {
  }

  ngOnInit(): void {
    this.cartService.cartProducts$.subscribe((products: CartProduct[]) => {
      this.cartProducts = products;
      if (products.length > 0) {
        this.isCartEmpty = false;
      } else {
        this.isCartEmpty = true;
      }
    });

    this.cartService.totalCartPrice$.subscribe((totalPrice: number) => {
      this.cartSubtotal = totalPrice;
      if (totalPrice >= 100) {
        this.cartTotalPrice = this.cartSubtotal;
        this.SHIPPING_PRICE = 0.00;
      }
      else {
        this.SHIPPING_PRICE = 2.99;
        this.cartTotalPrice = this.cartSubtotal + this.SHIPPING_PRICE;
      }

    });

    this.loggedUser = localStorage.getItem(this.USER_DATA_LABEL) ? JSON.parse(localStorage.getItem(this.USER_DATA_LABEL)) : JSON.parse(sessionStorage.getItem(this.USER_DATA_LABEL));
    this.profileSub$ = this.productsService.getProfile(this.loggedUser.userDto.email).subscribe((profileInfo: Profile) => {
      this.userProfile = profileInfo;
      this.orderFormGroup = this.formBuilder.group({
        address: [this.userProfile.userDetailsDto?.address, Validators.required],
        email: [this.userProfile.email, Validators.required],
        phone: [this.userProfile.userDetailsDto?.phone, Validators.required],
        ownerName: [this.userProfile.userDetailsDto?.paymentCardDto?.ownerName, Validators.required],
        cardNumber: [this.userProfile.userDetailsDto?.paymentCardDto?.number, Validators.required],
        expiryDate: [this.userProfile.userDetailsDto?.paymentCardDto?.expiryDate, Validators.required],
        cvvCvcCode: [this.userProfile.userDetailsDto?.paymentCardDto?.cvvCvcCode, Validators.required]
      });
    });
  }

  public changeProductQuantity(selectedProduct: Product, event: any): void {
    this.cartService.addOrUpdateCart(selectedProduct, event.target.value, true);
  }

  public onSubmit(): void {
    this.isSubmitted = true;
    if (this.orderFormGroup.valid) {
      this.assignFormDataToProfile(this.userProfile);
      this.buildCartOrderPayload();
    }
  }

  get form() {
    return this.orderFormGroup.controls;
  }

  public buildCartOrderPayload(): void {
    const cartOrder: CartOrder = new CartOrder(this.cartProducts, this.cartTotalPrice.toFixed(2), this.userProfile);
    this.currentOrderStatus = OrderStatus.InProgress;
    const dialogRef = this.dialog.open(CartOrderDialog, {
      width: '475px',
      position: {
        'top': '25px'
      },
      data: { orderStatus: this.currentOrderStatus, orderResponse: this.cartOrderResponse },
      backdropClass: "backdrop",
      panelClass: "order-dialog-container"
    });
    this.cartService.initiateCartOrder(cartOrder).subscribe(
      (orderResponse) => {
        this.cartOrderResponse = orderResponse;
        if (this.cartOrderResponse.outOfStockCartProducts.length > 0) {
          this.updateDialogData(dialogRef, OrderStatus.OutOfStockProducts);
        } else if (this.cartOrderResponse.errorMessage?.toLowerCase().includes(this.PAYMENT_ERROR_LABEL)) {
          this.updateDialogData(dialogRef, OrderStatus.PaymentUnsuccessful);
        } else {
          this.cartService.emptyCart();
          this.updateDialogData(dialogRef, OrderStatus.CompletedSuccessfully);
          this.cartService.sendConfirmationEmail(orderResponse.orderId, OrderMailAction.Confirmation).subscribe();
        }
      },
      (error) => {
        this.orderSuccessfullyProcessed = false;
        this.currentOrderStatus = OrderStatus.InternalServerError;
        this.updateDialogData(dialogRef, OrderStatus.InternalServerError);
      })

    const dialogSub$ = dialogRef.componentInstance.onConfirm.subscribe(() => {
      this.retryCartOrderPayload();
    })

    dialogRef.afterClosed().subscribe(result => {
      dialogSub$.unsubscribe();
    })
  }

  public retryCartOrderPayload(): void {
    this.cartService.updateCartWithInStockProducts(this.cartOrderResponse.inStockCartProducts);
    this.buildCartOrderPayload();
  }

  private updateDialogData(dialogRef: MatDialogRef<CartOrderDialog, any>, orderStatus: OrderStatus) {
    this.currentOrderStatus = orderStatus;
    if (dialogRef && dialogRef.componentInstance) {
      dialogRef.componentInstance.dialogData = { orderStatus: this.currentOrderStatus, orderResponse: this.cartOrderResponse };
    }
  }

  private assignFormDataToProfile(userProfile: Profile) {
    const profileForm = this.orderFormGroup.value;

    userProfile.userDetailsDto.address = profileForm.address;
    userProfile.userDetailsDto.phone = profileForm.phone;
    userProfile.userDetailsDto.place = profileForm.place;
    const paymentCardDto = {
      ownerName: profileForm.ownerName,
      number: profileForm.cardNumber,
      expiryDate: profileForm.expiryDate,
      cvvCvcCode: profileForm.cvvCvcCode
    }
    userProfile.userDetailsDto.paymentCardDto = paymentCardDto;
  }

  public onCardNumberInputChange(event: any): void {
    if (event.target.value.startsWith('4')) {
      this.cardImageResourcePath = "../../../assets/images/card/visa-logo.png";
    } else if (event.target.value.startsWith('5')) {
      this.cardImageResourcePath = "../../../assets/images/card/mastercard-logo.png";
    }
  }

}
