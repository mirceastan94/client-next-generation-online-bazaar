import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { HeaderConfig } from '../header/header.config';
import { CartProduct } from '../model/cart.model';
import { ItemResource, Product } from '../model/products.model';
import { CartService } from './cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  @Input()
  headerCfg: HeaderConfig;

  cartProducts: CartProduct[] = [];
  totalCartPrice: number;

  otherItems: ItemResource[] = [];
  checkoutResource: ItemResource;
  cartResource: ItemResource;
  removeItemResource: ItemResource;
  totalResource: ItemResource;
  viewCartResource: ItemResource;
  itemsResource: ItemResource;

  constructor(private cartService: CartService) {
  }

  ngOnInit(): void {
    this.headerCfg.headerResource.subscribe((currentItem: ItemResource) => {
      this.otherItems.push(currentItem);
      this.cartResource = this.otherItems.find(currentItem => currentItem.key === "label");
      this.itemsResource = this.otherItems.find(currentItem => currentItem.key === "items-label");
      this.removeItemResource = this.otherItems.find(currentItem => currentItem.key === "remove-item-label");
      this.totalResource = this.otherItems.find(currentItem => currentItem.key === "total-label");
      this.viewCartResource = this.otherItems.find(currentItem => currentItem.key === "view-label");
      this.checkoutResource = this.otherItems.find(currentItem => currentItem.key === "checkout-label");
    });
    this.cartService.cartProducts$.subscribe((cartProducts: CartProduct[]) => {
      this.cartProducts = cartProducts;
    });
    this.cartService.totalCartPrice$.subscribe((totalCartPrice: number) => {
      this.totalCartPrice = totalCartPrice;
    });
  }

  removeFromCart(product: Product): void {
    this.cartService.removeFromCart(product);
  }

}
