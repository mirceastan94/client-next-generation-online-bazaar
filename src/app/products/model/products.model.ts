import { IconName } from "@fortawesome/fontawesome-svg-core";

export class Product {
  id: string;
  name: string;
  description: string;
  category: string;
  subCategory: string;
  stockCount: number;
  brand: string;
  price: number;
  discountPrice: number;
  discountPercentage?: number;
  rating: number;
  imageData: string;
  popularity: string;
  introductoryDate: Date;
  isNew: boolean;
  headline: boolean;
  reviewsCount: number;
}

export class Criterion {
  name?: string;
  filterCriteria?: any;
  sortOperator?: "equal" | "greaterOrEqual";
  page: number;
  size: number;
  sortByField: string;
  order: string;
  campaignId?: string;
}

export class Headline {
  name: string;
  shortDescription: string;
  fullDescription?: string;
  imageLocation: string;
  uri: string;
  uriDescription: string;
}

export class DescriptionResource {
  name: string;
  description: string;
  icon: IconName;
}

export class ItemResource {
  key: string;
  name: string;
  uri: string;
  icon: IconName;
}

export class Category {
  key: string;
  name: string;
  subSections: Section[];

  constructor(key: string, name: string, subSections: Section[]) {
    this.key = key;
    this.name = name;
    this.subSections = subSections;
  }

}

export class Translation {
  key: string;
  label: string;

  constructor(key: string, label: string) {
    this.key = key;
    this.label = label;
  }
}

export class Section {
  key: string;
  name: string;
  imagePath: string;
  brands: Section[];

  constructor(name?: string) {
    this.name = name;
  }
}

export class Collection {
  key: string;
  name: string;
  description: string;
  uriDescription: string;
}
