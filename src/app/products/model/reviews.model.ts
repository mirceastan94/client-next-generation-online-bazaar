import { User } from "src/app/auth/model/user.model";

export class ReviewSummary {
  reviewDtos: Review[];
  reviewsCount: number;
  averageRating: number;
  excellent: number;
  good: number;
  average: number;
  bad: number;
  horrible: number;
}

export class Review {
  id: string;
  content: string;
  dateOfPosting: string;
  rating: number;
  verifiedPurchase: boolean;
  userDto: User;
  productId: string;

  constructor(content: string, dateOfPosting: string, rating: number, verifiedPurchase: boolean, userDto: User, productId: string) {
    this.content = content;
    this.dateOfPosting = dateOfPosting;
    this.rating = rating;
    this.verifiedPurchase = verifiedPurchase;
    this.userDto = userDto;
    this.productId = productId;
  }
}

export class ReviewInfo {
  category: string;
  starsArray: number[];
  count: number;

  constructor(category: string, rating: number[], count: number) {
    this.category = category;
    this.starsArray = rating;
    this.count = count;
  }

}
