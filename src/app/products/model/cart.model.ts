import { Product } from "./products.model";

export interface CartProduct {

  product: Product;
  quantity: number;
  totalPrice: number;

}
