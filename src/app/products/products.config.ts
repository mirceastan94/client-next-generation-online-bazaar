import { ReplaySubject } from "rxjs";
import { NgxTransService } from "src/app/shared/translate/translate.service";
import { Category, Collection, Criterion, Headline, Section } from "./model/products.model";
import { ProductsService } from "./products.service";

export class ProductsConfig {

  public headline = new ReplaySubject<Headline>();
  public categories = new ReplaySubject<Category>();
  public collections = new ReplaySubject<Collection>();

  public footerTranslations: any[] = [];
  public productsTranslations: any = [];
  public collectionsTranslations: any = [];

  private readonly PRODUCTS_LABEL = "products";
  private readonly INDEX_LABEL = "index";
  private readonly HEADLINE_LABEL = "headline";
  private readonly CATEGORIES_LABEL = "categories";
  private readonly SECTIONS_LABEL = "sections";
  private readonly BRANDS_LABEL = "brands";
  private readonly COLLECTIONS_LABEL = "collections";
  private readonly CATEGORIES_NAMES = Array.of("technology", "electronics", "cosmetics", "fashion", "books", "sport", "auto");
  private readonly COLLECTIONS_NAMES = Array.of("electronics", "cosmetics");
  public readonly SPECIAL_CATEGORIES_NAMES = Array.of("hot-deals", "recent-arrivals");
  public readonly SECTIONS_MAP = new Map()
    .set("technology", [{ label: "smartphones" }, { label: "computers" }, { label: "cameras" }, { label: "tvs" }])
    .set("electronics", [{ label: "washing-machines", imagePath: "../../assets/images/products/washing-machine.jpg", brands: ["lg", "samsung", "miele"] },
    { label: "vacuum-cleaners", imagePath: "../../assets/images/products/vacuum-cleaner.jpg", brands: ["roborock", "star-light"] }])

  constructor(private productsService: ProductsService, private translateService: NgxTransService) {
    this.buildCategoriesModel();
    this.buildCollectionsModel();
  }

  public criteriaModel: Criterion[] = [
    {
      name: "Default",
      filterCriteria: {},
      page: 0,
      size: 20,
      sortByField: "id",
      order: "ASC"
    },
    {
      name: "IndexTrending",
      filterCriteria: { "category": "Fashion, Books" },
      page: 0,
      size: 20,
      sortByField: "id",
      order: "ASC"
    },
    {
      name: "Electronics",
      filterCriteria: { "category": "Electronics" },
      page: 0,
      size: 20,
      sortByField: "id",
      order: "ASC"
    },
    {
      name: "Cosmetics",
      filterCriteria: { "category": "Cosmetics" },
      page: 0,
      size: 20,
      sortByField: "id",
      order: "ASC"
    },
    {
      name: "HotDeals",
      filterCriteria: { "popularity": "High" },
      page: 0,
      size: 20,
      sortByField: "id",
      order: "ASC"
    },
    {
      name: "RecentArrivals",
      filterCriteria: { "introductoryDate": "2021-01-01" },
      page: 0,
      size: 20,
      sortByField: "id",
      order: "ASC"
    },
    {
      name: "Headline",
      filterCriteria: { "isHeadline": "true" },
      page: 0,
      size: 1,
      sortByField: "id",
      order: "ASC"
    }
  ];

  public buildCategoriesModel(): void {
    this.translateService.getKeyTranslation(this.PRODUCTS_LABEL).subscribe((translatedStrings) => {
      this.productsTranslations = translatedStrings;
      const headlineCriterion = this.criteriaModel.find(criterion => criterion?.name?.toLowerCase() === this.HEADLINE_LABEL);
      this.productsService.retrieveProducts(headlineCriterion).subscribe((headlineProduct) => {
        this.headline.next(
          {
            name: this.productsTranslations[this.INDEX_LABEL][this.HEADLINE_LABEL].name,
            shortDescription: this.productsTranslations[this.INDEX_LABEL][this.HEADLINE_LABEL].shortDescription,
            fullDescription: this.productsTranslations[this.INDEX_LABEL][this.HEADLINE_LABEL].fullDescription,
            imageLocation: "../../../assets/images/products/slider-background.jpg",
            uri: headlineProduct[0].id,
            uriDescription: this.productsTranslations[this.INDEX_LABEL][this.HEADLINE_LABEL].uriDescription
          }
        );
      });

      this.CATEGORIES_NAMES.forEach(currentCategoryName => {
        const categoryName = this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentCategoryName].label
          ? this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentCategoryName].label
          : this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentCategoryName];
        const sections: any[] = this.SECTIONS_MAP.get(currentCategoryName);
        const subsections: Section[] = [];
        sections?.forEach(currentSection => {
          const sectionObj = new Section();
          sectionObj.key = currentSection.label;
          sectionObj.name = this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentCategoryName][this.SECTIONS_LABEL][currentSection.label].label ?
            this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentCategoryName][this.SECTIONS_LABEL][currentSection.label].label :
            this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentCategoryName][this.SECTIONS_LABEL][currentSection.label];
          sectionObj.imagePath = currentSection.imagePath;
          const brands: any[] = currentSection.brands;
          const translatedBrands: Section[] = [];
          brands?.forEach(currentBrand => {
            const translatedBrand = this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentCategoryName][this.SECTIONS_LABEL]
            [currentSection.label][this.BRANDS_LABEL][currentBrand];
            translatedBrands.push(new Section(translatedBrand));
          })
          sectionObj.brands = translatedBrands;
          subsections.push(sectionObj);
        })
        this.categories.next(
          {
            key: currentCategoryName,
            name: categoryName,
            subSections: subsections
          }
        )
      });
      this.SPECIAL_CATEGORIES_NAMES.forEach(currentSpecialCategoryName => {
        this.categories.next(
          {
            key: currentSpecialCategoryName,
            name: this.productsTranslations[this.INDEX_LABEL][this.CATEGORIES_LABEL][currentSpecialCategoryName],
            subSections: []
          }
        )
      })
    });
  }

  public buildCollectionsModel(): void {
    this.translateService.getKeyTranslation(this.PRODUCTS_LABEL).subscribe((translatedStrings) => {
      this.collectionsTranslations = translatedStrings;
      this.COLLECTIONS_NAMES.forEach(currentCollection => {
        const currentCollectionObj = this.collectionsTranslations[this.INDEX_LABEL][this.COLLECTIONS_LABEL].find((label: { key: string; }) => label.key === currentCollection);
        this.collections.next(
          {
            key: currentCollection,
            name: currentCollectionObj.name,
            description: currentCollectionObj.description,
            uriDescription: currentCollectionObj.uriDescription
          }
        );
      });
    });
  }
}

