import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { CartDetailsComponent } from './cart/cart-details/cart-details.component';
import { DetailsComponent } from './details/details.component';
import { GridComponent } from './grid/grid.component';
import { ProductsComponent } from './products.component';

const appRoutes: Routes = [
  { path: '', component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'cart-details', component: CartDetailsComponent, canActivate: [AuthGuard] },
  { path: ':id', component: DetailsComponent, canActivate: [AuthGuard] },
  { path: ':id/campaign/:campaignId', component: DetailsComponent, canActivate: [AuthGuard] },
  { path: 'category/:category', component: GridComponent, canActivate: [AuthGuard] },
  { path: 'subcategory/:subcategory', component: GridComponent, canActivate: [AuthGuard] },
  { path: 'special/:type', component: GridComponent, canActivate: [AuthGuard] },
  { path: 'campaign/:id', component: GridComponent, canActivate: [AuthGuard] },
  { path: 'brand/:brand', component: GridComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
