import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { AboutContactComponent } from './products/header/about-contact/about-contact.component';
import { ProfileComponent } from './products/profile/profile.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'about', component: AboutContactComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(module => { return module.DashboardModule})},
  { path: 'orders', loadChildren: () => import('./orders/orders.module').then((module) => { return module.OrdersModule })},
  { path: 'products', loadChildren: () => import('./products/products.module').then((module) => { return module.ProductsModule }) },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
