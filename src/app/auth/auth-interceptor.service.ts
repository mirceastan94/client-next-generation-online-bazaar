import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const localData = JSON.parse(localStorage.getItem("userData"));
    const sessionData = JSON.parse(sessionStorage.getItem("userData"));
    if (localData && localData.accessToken) {
      const clonedReq = req.clone({
        headers: req.headers.set("Authorization", "Bearer " + localData.accessToken)
      });
      return next.handle(clonedReq);
    } else if (sessionData && sessionData.accessToken) {
      const clonedReq = req.clone({
        headers: req.headers.set("Authorization", "Bearer " + sessionData.accessToken)
      });
      return next.handle(clonedReq);
    } else {
      return next.handle(req);
    }
  }

}
