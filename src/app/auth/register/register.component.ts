import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LogService } from 'src/app/shared/logging/log.service';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { PasswordValidator } from 'src/app/shared/utils/password-validator';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  errorMessage = "";
  alignClass = "language-toolbar";

  constructor(private formBuilder: FormBuilder, private authService: AuthService,
    private router: Router, private translateService: NgxTransService, private logger: LogService) {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  ngOnInit() {
    this.registerForm.setValidators(PasswordValidator.passwordsMatch("password", "confirmPassword"));
  }

  get form() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.valid) {
      this.logger.log("Signing up...");
      const formControls = this.registerForm.controls;
      this.authService.register(formControls.name.value, formControls.email.value, formControls.password.value).subscribe(
        (responseData) => {
          this.logger.log("User registered: " + responseData);
          this.router.navigate(['/login']);
        },
        (error) => {
          this.logger.log(error);
          this.errorMessage = this.translateService.getMessageTranslation(error);
        });
    }
  }

  changeLanguage(langValue: string) {
    this.translateService.applyLanguage(langValue);
  }

}
