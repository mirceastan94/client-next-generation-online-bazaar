import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { Subscription } from 'rxjs';
import { LogService } from 'src/app/shared/logging/log.service';
import { NgxTransService } from 'src/app/shared/translate/translate.service';
import { AuthService } from '../auth.service';
import { LoginResponseData } from '../model/login.response.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  submitted: boolean = false;
  registered: boolean = false;
  errorMessage: string = "";
  alignClass: string = "language-toolbar-right";

  private user!: SocialUser;
  private loggedIn: boolean = false;
  private registeredSub: Subscription;

  constructor(private formBuilder: FormBuilder, private socialAuthService: SocialAuthService, private router: Router,
    private authService: AuthService, private logger: LogService, private translateService: NgxTransService) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      rememberMe: ['']
    });
  }

  ngOnInit() {
    this.socialAuthService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });
    this.registeredSub = this.authService.userWasRegistered.subscribe((userGotRegistered) => {
      this.registered = userGotRegistered;
    });
  }

  ngOnDestroy() {
    this.registeredSub.unsubscribe();
  }

  get form() {
    return this.loginForm.controls;
  }

  socialSignIn(provider: string) {
    this.logger.log("Authenticating via social login, namely " + provider);
    this.socialAuthService.signIn(provider == "Google" ? GoogleLoginProvider.PROVIDER_ID : FacebookLoginProvider.PROVIDER_ID)
      .then((user) => {
        this.authService.login(user, true).subscribe(
          (responseData) => {
            this.completeAuthentication(responseData);
          },
          (error) => {
            this.handleAuthenticationErrors(error);
          });
      })
      .catch(ex => {
        this.logger.log("Unsuccessful social login: " + JSON.stringify(ex));
      })
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.logger.log("Authenticating...");
      this.authService.login(this.loginForm.value, this.loginForm.get('rememberMe').value).subscribe(
        (responseData) => {
          this.completeAuthentication(responseData);
        },
        (error) => {
          this.handleAuthenticationErrors(error);
        });
    }
  }

  private completeAuthentication(responseData: LoginResponseData) {
    this.logger.log("Authentication completed: " + JSON.stringify(responseData));
    this.router.navigateByUrl('/products');
    this.errorMessage = "";
    this.loginForm.reset();
  }

  private handleAuthenticationErrors(error: any) {
    this.logger.log(error);
    this.errorMessage = this.translateService.getMessageTranslation(error);
  }

}
