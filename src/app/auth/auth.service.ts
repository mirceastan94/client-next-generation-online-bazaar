import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, tap } from "rxjs/operators";
import { BehaviorSubject, throwError } from "rxjs";
import { User } from "./model/user.model";
import { LoginResponseData as LoginResponseData } from "./model/login.response.model";
import { environment } from "./../../environments/environment";
import { RegisterResponseData } from "./model/register.response.model";
import { LogService } from "../shared/logging/log.service";
import { SocialUser } from "angularx-social-login";
import { Router } from "@angular/router";
import { NgxTransService } from "../shared/translate/translate.service";

@Injectable({ providedIn: 'root' })
export class AuthService {

  user = new BehaviorSubject<User>(null);
  userWasRegistered = new BehaviorSubject<boolean>(null);
  private tokenExpirationTimer: any;

  constructor(private httpClient: HttpClient, private logger: LogService, private router: Router, private translateService: NgxTransService) {
  }

  register(name: string, email: string, password: string) {
    return this.httpClient.post<RegisterResponseData>(environment.productsApiUrl.concat("/user/register"), {
      name: name, email: email, password: password
    }).pipe(catchError(this.handleError), tap(registerResData => {
      this.handleRegistration(registerResData);
    }));
  }

  login(user: any, rememberMe: boolean) {
    user["language"] = this.translateService.getCurrentLanguage();
    return this.httpClient.post<LoginResponseData>(environment.productsApiUrl.concat(user instanceof SocialUser ? "/user/social-login" : "/user/login"),
      user
    ).pipe(catchError(this.handleError), tap(loginResData => {
      this.handleAuthentication(loginResData, rememberMe);
    }));
  }

  logout() {
    this.user.next(null);
    localStorage.removeItem('userData');
    sessionStorage.removeItem('userData');
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;
    this.router.navigate(['/login']);
  }

  autoLogin() {
    const userData: {
      accessToken: string, expiryDate: Date, tokenType: string, userDto: User
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return;
    }
    const loadedUser = new User(userData.userDto.id, userData.userDto.name, userData.userDto.email, userData.userDto.role, undefined);
    if (userData.accessToken) {
      this.user.next(loadedUser);
      const expirationDuration = new Date(userData.expiryDate).getTime() - new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  autoLogout(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => { this.logout() }, expirationDuration);
  }

  private handleRegistration(responseData: RegisterResponseData) {
    this.userWasRegistered.next(true);
    this.logger.log(responseData.message);
    return responseData.success;
  }

  private handleAuthentication(responseData: LoginResponseData, rememberMe: boolean) {
    // retrieves logged user information
    const loggedUser = responseData.userDto;
    this.user.next(loggedUser);
    this.userWasRegistered.next(false);

    // sets the auto logout based on the remaining expiration period
    const expiresIn = new Date(responseData.expiryDate).getTime() - new Date().getTime();
    this.autoLogout(expiresIn);

    // sets the correct storage for sending the token in the future, depending whether "remember me" checkbox has been selected or not
    rememberMe ? localStorage.setItem('userData', JSON.stringify(responseData)) : sessionStorage.setItem('userData', JSON.stringify(responseData));
  }

  private handleError(errorRes: HttpErrorResponse) {
    let errorMessage = 'unknown-error';
    if (!errorRes.error || !errorRes.error.message) {
      return throwError(errorMessage);
    }
    switch (errorRes.error.message) {
      case 'Another user with the same email exists in the database already!':
        errorMessage = 'email-already-exists';
        break;
      case 'Bad credentials':
        errorMessage = 'invalid-credentials';
        break;
    }
    return throwError(errorMessage);
  }

}
