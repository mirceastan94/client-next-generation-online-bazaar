export class User {
  constructor(public id: string, public name: string, public email: string, public role: string, public userDetailsDto: UserDetails) {
  }
}

export class UserDetails {
  constructor(public picture: string) {
  }
}
