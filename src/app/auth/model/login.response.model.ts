import { User } from "./user.model";

export interface LoginResponseData {
  tokenType: string; accessToken: string; email: string; expiryDate: string, userDto: User;
}
